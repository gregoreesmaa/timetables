<?php
	/*
	* Inserts or updates timetable into database.
	* Receives REQUEST request with variables: 
	*	create - 0 (update) or 1 (create)
	*	name - text up to 48 characters (name of the period/timetable)
	*	start - YYYY-MM-DD format (start date of period, inclusive)
	*	end - YYYY-MM-DD format (end date of period, inclusive)
	*	file - (preferably xml) file (containing timetable data in ASC Oman XML format)
	*	periodid (only if create = 0 aka updating) - numerical value (representing the ID of the period to be updated)
	* OUTPUT: "periodid" (or "Error: errormsg")
	*	periodid - numerical value (ID of the added period)
	*/
	if ($level < 5)
		die();

	$create = $_POST['create'] == 1;
	$name = mysqli_real_escape_string($link, mb_convert_encoding($_POST['name'], "UTF-8"));
	$filename = mysqli_real_escape_string($link, $_FILES["file"]["name"]);
	$start = mysqli_real_escape_string($link, $_POST['start']);
	$end = mysqli_real_escape_string($link, $_POST['end']);

	if ($create) {
        if (empty($_FILES['file']['name'])) {
        	die("Error: file is required to be uploaded");
        }
		mysqli_query($link, "INSERT INTO `".$db_prefix."periods` (`id`, `name`, `filename`, `start`, `end`, `created`, `last_modified`) VALUES (NULL, '".$name."', '".$filename."', '".$start."', '".$end."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);") or die ("Error: (3.1.1) ".mysqli_error($link));
		$periodid = $link->insert_id;
	} else {
		$periodid = $_POST['periodid'];

		// Check for ID validness
		if (!is_numeric($periodid)) {
			die("Error: periodid ".$periodid." isn't numeric");
		}

		// Verify existance of specified periodid
		$periodLookup = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods` WHERE `id` = ".$periodid.";") or die ("Error: (3.1.2) ".mysqli_error($link));
		$periodCount = mysqli_num_rows($periodLookup);
		mysqli_free_result($periodLookup);
		if ($periodCount != 1) {
			die("Error: period with id ".$periodid." doesn't exist");
		}

		// TODO better updating
		// Update metadata
		mysqli_query($link, "UPDATE `".$db_prefix."periods` SET `name` = '".$name."', `start` = '".$start."', `end` = '".$end."' WHERE `id` = ".$periodid.";") or die ("Error: (3.1.3) ".mysqli_error($link));
        
        if (empty($_FILES['file']['name'])) {
            // No file selected, not updating timetable contents
            mysqli_close($link);
            die($periodid);
        }
        
		// REMOVING PREVIOUS DATA
		mysqli_query($link, "DELETE FROM `".$db_prefix."periods_lessonperiods` WHERE `periodid` = ".$periodid.";") or die ("Error: (3.1.4) ".mysqli_error($link));
		mysqli_query($link, "DELETE FROM `".$db_prefix."periods_schedules_forms` WHERE `scheduleid` IN (SELECT id FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$periodid.");") or die ("Error: (3.1.5) ".mysqli_error($link));
        mysqli_query($link, "DELETE FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$periodid.";") or die ("Error: (3.1.6) ".mysqli_error($link));
	}

	$xml_tmp_name = $_FILES['file']['tmp_name'];
	$xml_name = $periodid."_".$_FILES['file']['name'];
	$xml_src = file_get_contents($xml_tmp_name);

	$encodingStart = strpos($xml_src, "encoding=\"") + 10;
	$encodingEnd = strpos($xml_src, "\"", $encodingStart);
	$encoding = substr($xml_src, $encodingStart, $encodingEnd - $encodingStart);
	if ($encoding === "windows-1257") {
		$encoding = "ISO-8859-13"; // Because of similarity and wider range of support
	}

	$xml_src = mb_convert_encoding($xml_src, "UTF-8", $encoding);
	$xml_src = substr_replace($xml_src, "UTF-8", $encodingStart, $encodingEnd - $encodingStart);

	// Move uploaded XML for Karl.
	if (!is_dir($timetable_xml_dir)) {
    	mkdir($timetable_xml_dir, 0777, true);
	}
	move_uploaded_file($xml_tmp_name, $timetable_xml_dir.$xml_name);

	$doc = new DOMDocument();

	libxml_use_internal_errors(true);
	$doc->strictErrorChecking = FALSE;
	$doc->recover = TRUE;
	$doc->loadXML($xml_src);
	libxml_clear_errors();

	$days = $doc->getElementsByTagName("days")->item(0);
    $dayidToWeekday = array();
	foreach ($days->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = $n->getAttribute("day");
			if (is_numeric($id)) {
				$name_u = $n->getAttribute("name");
				$name_u_lowercase = trim(strtolower($name_u));

				$weekday = -1;
				$firstOne = substr($name_u_lowercase, 0, 1);
				$firstTwo = substr($name_u_lowercase, 0, 2);
				// Works for: Estonian, German, Finnish, English, Russian
				if ($firstTwo === "mo" || $firstTwo === "ma" || $firstOne === "e" || $firstTwo === "по") {
					$weekday = 1;
				} else if ($firstTwo === "tu" || $firstTwo === "di" || $firstTwo === "ti" || $firstTwo === "te" || $firstTwo === "вт") {
					$weekday = 2;
				} else if ($firstOne === "w" || $firstOne === "m" || $firstOne === "k" || $firstTwo === "ср") {
					$weekday = 3;
				} else if ($firstTwo === "th" || $firstTwo === "to" || $firstOne === "n" || $firstOne === "ч") {
					$weekday = 4;
				} else if ($firstOne === "f" || $firstOne === "r" || $firstOne === "pe" || $firstTwo === "пя") {
					$weekday = 5;
				} else if ($firstTwo === "sa" || $firstOne === "l" || $firstTwo === "су") {
					$weekday = 6;
				} else if ($firstOne === "s" || $firstOne === "p" || $firstTwo === "во") {
					$weekday = 7;
				} else {
					die("Error: unable to categorize day with name: $name_u; please contact the author of this system.");
				}
                $dayidToWeekday[$id] = $weekday;
			}
		}
	}

	$lessonperiods = $doc->getElementsByTagName("periods")->item(0);
    $query = "";
	foreach ($lessonperiods->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = $n->getAttribute("period");
			if (is_numeric($id)) {
				$starttime = mysqli_real_escape_string($link, $n->getAttribute("starttime"));
				$endtime = mysqli_real_escape_string($link, $n->getAttribute("endtime"));
                $query .= "('".$id."', '".$starttime."', '".$endtime."', '".$periodid."'), ";
			}
		}
	}
	if (!empty($query)) {
    	mysqli_query($link, "REPLACE INTO `".$db_prefix."periods_lessonperiods` (`id`, `starttime`, `endtime`, `periodid`) VALUES ".rtrim($query, ", ")) or die ("Error: (3.2.1) ".mysqli_error($link));
    }

	$subjects = $doc->getElementsByTagName("subjects")->item(0);
    $periodsubjectidToSubjectid = array();
	foreach ($subjects->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = substr($n->getAttribute("id"), 1);
			if (is_numeric($id)) {
				$name = mysqli_real_escape_string($link, $n->getAttribute("name"));
				$alias = mysqli_real_escape_string($link, preg_replace('/[^\p{L}\d]/u', '', mb_strtolower($n->getAttribute("name"), "UTF-8")));

				// Add subject to subjects table
				mysqli_query($link, "INSERT INTO `".$db_prefix."subjects` (`id`, `alias`, `name`) VALUES (NULL, '".$alias."', '".$name."') ON DUPLICATE KEY UPDATE `name` = '".$name."'") or die ("Error: (3.2.2.1)".mysqli_error($link));
				$subjectid = $link->insert_id;
				// Row already exists, get ID
				if ($subjectid === 0) {
					$subjectidQuery = mysqli_query($link, "SELECT `id` FROM `".$db_prefix."subjects` WHERE `name`='".$name."'") or die ("Error: (3.2.2.2) ".mysqli_error($link));
					$subjectid = mysqli_fetch_array($subjectidQuery)[0];
					mysqli_free_result($subjectidQuery);
				}
                $periodsubjectidToSubjectid[$id] = $subjectid;
			}
		}
	}

	$teachers = $doc->getElementsByTagName("teachers")->item(0);
    $periodteacheridToPersonid = array();
	foreach ($teachers->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = substr($n->getAttribute("id"), 1);
			$color = hexdec(substr($n->getAttribute("color"), 1));
			if (is_numeric($id) && (is_numeric($color) || empty($color))) {
				$name = mysqli_real_escape_string($link, $n->getAttribute("name"));
				$alias = mysqli_real_escape_string($link, preg_replace('/[^\p{L}\d]/u', '', mb_strtolower($n->getAttribute("name"), "UTF-8")));
				// Add teacher to people's table
				mysqli_query($link, "INSERT INTO `".$db_prefix."people` (`id`, `alias`, `name`, `color`) VALUES (NULL, '".$alias."', '".$name."', '".$color."') ON DUPLICATE KEY UPDATE `name` = '".$name."', `color` = '".$color."'") or die ("Error: (3.2.3) ".mysqli_error($link));
				$personid = $link->insert_id;
				// Row already exists, get ID
				if ($personid === 0) {
					$personidQuery = mysqli_query($link, "SELECT `id` FROM `".$db_prefix."people` WHERE `name`='".$name."'") or die ("Error: (3.2.4) ".mysqli_error($link));
					$personid = mysqli_fetch_array($personidQuery)[0];
					mysqli_free_result($personidQuery);
				}
				$periodteacheridToPersonid[$id] = $personid;
			}
		}
	}

	$classrooms = $doc->getElementsByTagName("classrooms")->item(0);
    $periodclassroomidToClassroomid = array();
	foreach ($classrooms->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = substr($n->getAttribute("id"), 1);
			if (is_numeric($id)) {
				$name = mysqli_real_escape_string($link, $n->getAttribute("name"));
				$alias = mysqli_real_escape_string($link, preg_replace('/[^\p{L}\d]/u', '', mb_strtolower($n->getAttribute("name"), "UTF-8")));

				// Add classroom to classrooms table
				mysqli_query($link, "INSERT INTO `".$db_prefix."classrooms` (`id`, `alias`, `name`) VALUES (NULL, '".$alias."', '".$name."') ON DUPLICATE KEY UPDATE `name` = '".$name."'") or die ("Error: (3.3.1)".mysqli_error($link));
				$classroomid = $link->insert_id;
				// Row already exists, get ID
				if ($classroomid === 0) {
					$classroomidQuery = mysqli_query($link, "SELECT `id` FROM `".$db_prefix."classrooms` WHERE `name`='".$name."'") or die ("Error: (3.3.2) ".mysqli_error($link));
					$classroomid = mysqli_fetch_array($classroomidQuery)[0];
					mysqli_free_result($classroomidQuery);
				}
                $periodclassroomidToClassroomid[$id] = $classroomid;
			}
		}
	}

	$forms = $doc->getElementsByTagName("classes")->item(0);
    $periodformidToFormid = array();
	foreach ($forms->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$id = substr($n->getAttribute("id"), 1);
			if (is_numeric($id)) {
				$name = mysqli_real_escape_string($link, $n->getAttribute("name"));
				$alias = mysqli_real_escape_string($link, preg_replace('/[^\p{L}\d]/u', '', mb_strtolower($n->getAttribute("name"), "UTF-8")));

				// Add class to forms table
				mysqli_query($link, "INSERT INTO `".$db_prefix."forms` (`id`, `alias`, `name`) VALUES (NULL, '".$alias."', '".$name."') ON DUPLICATE KEY UPDATE `name` = '".$name."'") or die ("Error: (3.4.1)".mysqli_error($link));
				$formid = $link->insert_id;
				// Row already exists, get ID
				if ($formid === 0) {
					$formidQuery = mysqli_query($link, "SELECT `id` FROM `".$db_prefix."forms` WHERE `name`='".$name."'") or die ("Error: (3.4.2) ".mysqli_error($link));
					$formid = mysqli_fetch_array($formidQuery)[0];
					mysqli_free_result($formidQuery);
				}
                $periodformidToFormid[$id] = $formid;
			}
		}
	}

	$schedules = $doc->getElementsByTagName("TimeTableSchedules")->item(0);
	$ai_query = mysqli_query($link, "SHOW TABLE STATUS LIKE '".$db_prefix."periods_schedules'") or die ("Error: (3.5.0) ".mysqli_error($link));
	$ai_row = mysqli_fetch_assoc($ai_query); 
	$ai_value = (int) $ai_row["Auto_increment"]; 
	$schedules_query = "";
    $forms_query = "";
	foreach ($schedules->childNodes as $n) {
		if ($n->nodeType === XML_ELEMENT_NODE) {
			$dayid = $n->getAttribute("DayID");
			$lessonperiodid = $n->getAttribute("Period");
			$subjectid = str_replace('*', '', $n->getAttribute("SubjectGradeID"));
			$teacherid = str_replace('*', '', $n->getAttribute("TeacherID"));
			$schoolroomid = str_replace('*', '', $n->getAttribute("SchoolRoomID"));
			if (is_numeric($dayid) && (is_numeric($lessonperiodid) && $lessonperiodid != "-1") && is_numeric($subjectid) && (is_numeric($teacherid) || empty($teacherid)) && (is_numeric($schoolroomid) || empty($schoolroomid))) {
				$classids = explode(",*", substr($n->getAttribute("ClassID"), 1));
				if (!empty($teacherid)) $teacher = "'".$periodteacheridToPersonid[$teacherid]."'";
				else $teacher = "NULL";
				if (!empty($schoolroomid)) $schoolroom = "'".$periodclassroomidToClassroomid[$schoolroomid]."'";
				else $schoolroom = "NULL";
				$schedules_query .= "(NULL, '".$dayidToWeekday[$dayid]."', '".$lessonperiodid."', ".$schoolroom.", ".$teacher.", '".$periodsubjectidToSubjectid[$subjectid]."', '".$periodid."'), ";
				$scheduleid = $ai_value++;
				foreach ($classids as $classid) $forms_query .= "('".$periodformidToFormid[$classid]."', '".$scheduleid."'), ";
			}
		}
	}            	
	if (!empty($schedules_query)) {
		mysqli_query($link, "INSERT INTO `".$db_prefix."periods_schedules` (`id`, `weekday`, `lessonperiodid`, `classroomid`, `teacherid`, `subjectid`, `periodid`) VALUES ".rtrim($schedules_query, ", ")) or die ("Error: (3.5.1) ".mysqli_error($link));
	}
	if (!empty($forms_query)) {
        mysqli_query($link, "INSERT INTO `".$db_prefix."periods_schedules_forms` (`formid`, `scheduleid`) VALUES ".rtrim($forms_query, ", ")) or die ("Error: (3.5.2) ".mysqli_error($link));
    }
	$result = array("success" => "true", "id" => $periodid);
	$response = json_encode($result);
	echo $response;
?>