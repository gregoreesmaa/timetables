<?php
	/*
	* Defines constants and methods used in different backend actions.
	*/
	ini_set( 'default_charset', 'UTF-8' );
	setlocale(LC_ALL, 'et_EE', 'et', 'EE', 'ET', 'Estonia');

	$school_name = "Poska"; // The shorter the better
	$timetable_xml_dir = "../xml/"; // For storing original uploaded timetable files
	$cache_dir = "./cache/"; // For caching requests
	$local_timezone_str = "Europe/Tallinn"; // For calendar export, timezone rule will also have to be changed at iCalendar export. index.html also sets the timezone which has to be changed.
	
    $whitelist = array(
	    '127.0.0.1',
	    '::1'
	);

	// Database connection
	$db_servername = "db4free.net"; // Usually localhost
	$db_username = "poska_timetables";
	$db_password = "maakera147";
	$db_database = "poska_timetables";
	$db_prefix = "tt_"; // Can be left to "tt_"
	
	$stuudium_auth_url = "https://api.ope.ee/v1/authenticate";
	$stuudium_subdomain = "jpg";
	$stuudium_client_id = "E8sKQThfJh4LlfhcEq6z";
	$stuudium_client_secret = "t0MHksd1LtHqViSNGZvlgpNMNhmLjG";

	function connectDB() {
		global $db_servername, $db_username, $db_password, $db_database, $db_prefix, $link;
		if (isset($link))
			return;
		// Create connection
		$link = new mysqli($db_servername, $db_username, $db_password, $db_database);
		// Check connection
		if ($link->connect_error) {
			die("Error: Connection failed: ".$link->connect_error);
		} 
		/* change character set to utf8 */
		if (!$link->set_charset("utf8")) {
			printf("Error: Error loading character set utf8: %s\n", $link->error);
			exit();
		}
	}

    function mb_ucfirst($str, $encoding = "UTF-8") {
		$first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
		$str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
	  	$str = $first_letter . $str_end;
	  	return $str;
    }
?>