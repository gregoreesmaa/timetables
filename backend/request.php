<?php
    require_once "./config.php";
    $cache_file = $cache_dir;
    $key = array_keys($_GET);
    $size = sizeOf($key);
    for ($i = 0; $i < $size; $i++) $cache_file.= "_".$_GET[$key[$i]];
    $q = $_GET["q"];
    if ($q === "export-ical") {
    	header("Content-type:text/calendar");
    	header("Content-disposition: attachment; filename=icalexport.ics");
    }
    if (file_exists($cache_file)) print file_get_contents($cache_file);
    else {
        connectDB();
        switch ($q) {
            case "primary":
                /*
                * Primary DATA.
                * Fetches all general people, general forms, general subjects and general classrooms from the database.
                */
                $query = mysqli_query($link, "SELECT id, name, color, IF(`id` IN (SELECT teacherid FROM `".$db_prefix."periods_schedules`) OR `id` IN (SELECT teacherid FROM `".$db_prefix."periods_schedules_override`), true, false) AS public FROM `".$db_prefix."people`") or die ("Error: (1.1) ".mysqli_error($link));
                $people = array();
                while ($r = mysqli_fetch_assoc($query)) {
                    // Convert rgb int color to hex
                    $r["color"] = "#".sprintf("%06X", $r["color"]);
                    $r["type"] = "person";
                    $people[$r["id"]] = $r;
                }
                mysqli_free_result($query);
                $query = mysqli_query($link, "SELECT id, name, IF(`id` IN (SELECT formid FROM `".$db_prefix."periods_schedules_forms`) OR `id` IN (SELECT formid FROM `".$db_prefix."periods_schedules_override_forms`), true, false) AS public FROM `".$db_prefix."forms`") or die ("Error: (1.2) ".mysqli_error($link));
                $forms = array();
                while ($r = mysqli_fetch_assoc($query)) {                    
                    $r["type"] = "form";
                    $forms[$r["id"]] = $r;
                }
                mysqli_free_result($query);
                $query = mysqli_query($link, "SELECT id, name, IF(`id` IN (SELECT subjectid FROM `".$db_prefix."periods_schedules`), true, false) AS public FROM `".$db_prefix."subjects`") or die ("Error: (1.3) ".mysqli_error($link));
                $subjects = array();
                while ($r = mysqli_fetch_assoc($query)) {
                    // First letter is capital
                    $r["name"] = mb_ucfirst($r["name"]);                    
                    $r["type"] = "subject";
                    $subjects[$r["id"]] = $r;
                }
                mysqli_free_result($query);
                $query = mysqli_query($link, "SELECT id, name, IF(`id` IN (SELECT classroomid FROM `".$db_prefix."periods_schedules`) OR `id` IN (SELECT classroomid FROM `".$db_prefix."periods_schedules_override`), true, false) AS public FROM `".$db_prefix."classrooms`") or die ("Error: (1.3) ".mysqli_error($link));
                $classrooms = array();
                while ($r = mysqli_fetch_assoc($query)) {                    
                    $r["type"] = "classroom";
                    $classrooms[$r["id"]] = $r;
                }
                mysqli_free_result($query);
                $result = array("people" => $people, "forms" => $forms, "subjects" => $subjects, "classrooms" => $classrooms); // Preparing an array for people and forms
				if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
				    $response = json_encode($result, JSON_UNESCAPED_UNICODE);
				} else {
					$response = json_encode($result);
				}
                break;
            case "range":
                /*
                * Secondary DATA.
                * Fetches periods, holidays and events in specified time period. Returns them in JSON format.
                */            
                $start = mysqli_real_escape_string($link, $_GET['start']);
                $end = mysqli_real_escape_string($link, $_GET['end']);
                $result = array("periods" => array(), "holidays" => array(), "events" => array()); // Preparing an array for periods, holidays and events
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods` WHERE `start` < DATE_ADD('".$end."', INTERVAL 1 DAY) AND `end` >= '".$start."'") or die ("Error: (2.1) ".mysqli_error($link));
                while ($r = mysqli_fetch_assoc($query)) {
                    // Attach lessonperiods 
                    $query2 = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_lessonperiods` WHERE `periodid` = '".$r["id"]."';") or die ("Error: (2.1.2 '".$data."') ".mysqli_error($link));
                    while ($r2 = mysqli_fetch_assoc($query2)) {
                        $r["lessonperiods"][$r2["id"]] = $r2;
                    }
                    mysqli_free_result($query2);
                    $result["periods"][$r["id"]] = $r;
                }
                mysqli_free_result($query);
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."holidays` WHERE `start` < DATE_ADD('".$end."', INTERVAL 1 DAY) AND `end` >= '".$start."'") or die ("Error: (2.2) ".mysqli_error($link));
                while ($r = mysqli_fetch_assoc($query)) $result["holidays"][$r["id"]] = $r;
                mysqli_free_result($query);
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."events` WHERE `start` < DATE_ADD('".$end."', INTERVAL 1 DAY) AND `end` >= '".$start."'") or die ("Error: (2.3) ".mysqli_error($link));
                $eventids = "";
                while ($r = mysqli_fetch_assoc($query)) {
                    // Convert rgb int color to hex
                    $r["color"] = "#".sprintf("%06X", $r["color"]);
                    // Add arrays for participants
                    $r["forms"] = array();
                    $r["people"] = array();
                    $result["events"][$r["id"]] = $r;
                    $eventids.= $r["id"].',';
                }
                $eventids = rtrim($eventids, ',');
                mysqli_free_result($query);
                if (!empty($eventids)) {
                    // Add participating forms and people to events
                    $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."events_forms` WHERE `eventid` IN (".$eventids.")") or die ("Error: (2.4) ".mysqli_error($link));
                    while ($r = mysqli_fetch_assoc($query)) $result["events"][$r["eventid"]]["forms"][] = &$r["formid"];
                    mysqli_free_result($query);
                    $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."events_people` WHERE `eventid` IN (".$eventids.")") or die ("Error: (2.5) ".mysqli_error($link));
                    while ($r = mysqli_fetch_assoc($query)) $result["events"][$r["eventid"]]["people"][] = &$r["personid"];
                    mysqli_free_result($query);
                }
				if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
				    $response = json_encode($result, JSON_UNESCAPED_UNICODE);
				} else {
					$response = json_encode($result);
				}
                break;
            case "period":
                /*
                * Print DATA.
                * Fetches lessonperiods for the specified period from the database.
                */
                $periodid = $_GET['periodid'];
                if (!is_numeric($periodid)) die("Error: periodid ".$periodid." isn't numeric");
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods` WHERE `id` = '".$periodid."'") or die ("Error: (2.1) ".mysqli_error($link));
                $result = mysqli_fetch_assoc($query);
                mysqli_free_result($query);
                // Attach lessonperiods 
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_lessonperiods` WHERE `periodid` = '".$periodid."';") or die ("Error: (2.1.2 '".$data."') ".mysqli_error($link));
                while ($r = mysqli_fetch_assoc($query)) {
                    $result["lessonperiods"][$r["id"]] = $r;
                }
                mysqli_free_result($query);
				if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
				    $response = json_encode($result, JSON_UNESCAPED_UNICODE);
				} else {
					$response = json_encode($result);
				}
                break;
            case "filtered":
                /*
                * Quaternary DATA.
                * Fetches schedules of the specified period, that match the filter, from the database. 
                */
                // Recursive key sort
                function recur_ksort(&$array) {
                   foreach ($array as &$value) {
                      if (is_array($value)) recur_ksort($value);
                   }
                   return ksort($array);
                }
                $periodid = $_GET['periodid'];
                $filter_type = mysqli_real_escape_string($link, $_GET['filter_type']); // form, person, subject or classroom
                $id = $_GET['id']; // id of the form, person, subject or classroom
                if (!is_numeric($periodid)) die("Error: periodid ".$periodid." isn't numeric");
                else if (!is_numeric($id)) die("Error: specified ID ".$id." isn't numeric");
                $result = array();
                $schedules = array();
                if ($filter_type === "form") $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE (`id` IN (SELECT scheduleid FROM `".$db_prefix."periods_schedules_forms` WHERE `formid` = ".$id.") OR `id` IN (SELECT scheduleid FROM `".$db_prefix."periods_schedules_override` WHERE `id` IN (SELECT overrideid FROM `".$db_prefix."periods_schedules_override_forms` WHERE `formid` = ".$id."))) AND `periodid` = ".$periodid.";") or die ("Error: (4.1) ".mysqli_error($link));
                else if ($filter_type === "person") $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$periodid." AND (`teacherid` = ".$id." OR `id` IN (SELECT scheduleid FROM `".$db_prefix."periods_schedules_override` WHERE `teacherid` = ".$id."));") or die ("Error: (4.2.0) ".mysqli_error($link));
                else if ($filter_type === "subject") $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE `subjectid` = ".$id." AND `periodid` = ".$periodid.";") or die ("Error: (4.2.1) ".mysqli_error($link));
                else if ($filter_type === "classroom") $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$periodid." AND (`classroomid` = ".$id." OR `id` IN (SELECT scheduleid FROM `".$db_prefix."periods_schedules_override` WHERE `classroomid` = ".$id."));") or die ("Error: (4.2.2) ".mysqli_error($link));
                // else { $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE `".$filter_type."id` = ".$id." AND `periodid` = ".$periodid.";") or die ("Error: (3) ".mysqli_error($link)); }
                $scheduleids = "";
                while($r = mysqli_fetch_assoc($query)) {
                    $r["formids"] = array();
                    $r["overrides"] = array();
                    $schedules[$r["id"]] = $r;
                    $scheduleids.= $r["id"].',';
                }
                $scheduleids = rtrim($scheduleids, ',');
                mysqli_free_result($query);
                if (!empty($scheduleids)) {
                    // Add participating forms
                    $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules_forms` WHERE `scheduleid` IN (".$scheduleids.")") or die ("Error: (4.3) ".mysqli_error($link));
                    while ($r = mysqli_fetch_assoc($query)) $schedules[$r["scheduleid"]]["formids"][] = &$r["formid"];
                    mysqli_free_result($query);

                    // Add overrides
                    $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules_override` WHERE `scheduleid` IN (".$scheduleids.") ORDER BY `startweek` DESC") or die ("Error: (4.4) ".mysqli_error($link));
                    $scheduleOverrideIds = array();
                    $overrideids = "";
                    while ($r = mysqli_fetch_assoc($query)) {
                        $r["formids"] = array();
                        $schedules[$r["scheduleid"]]["overrides"][$r["id"]] = $r;
                        $scheduleOverrideIds[$r["id"]] = $r["scheduleid"];
                        $overrideids.= $r["id"].',';
                    }
                    $overrideids = rtrim($overrideids, ',');
                    mysqli_free_result($query);

                    if (!empty($overrideids)) {
                        $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules_override_forms` WHERE `overrideid` IN (".$overrideids.")") or die ("Error: (4.5) ".mysqli_error($link));
                        while ($r = mysqli_fetch_assoc($query)) {
                            $schedules[$scheduleOverrideIds[$r["overrideid"]]]["overrides"][$r["overrideid"]]["formids"][] = &$r["formid"];
                        }
                        mysqli_free_result($query);
                    }
                }
                // Categorize into days and lessonperiods, so the browser needs to do less work.
                foreach ($schedules as $scheduleid => &$schedule) {
                    $result[$schedule['weekday']][$schedule['lessonperiodid']][] = $schedule;
                }
                recur_ksort($result);
				if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
				    $response = json_encode($result, JSON_UNESCAPED_UNICODE);
				} else {
					$response = json_encode($result);
				}
                break;
            case "export-ical":
            	/*
            	* Exported data
            	*/

	            // Get classrooms
                $query = mysqli_query($link, "SELECT id, name FROM `".$db_prefix."classrooms`") or die ("Error: (5.0.1) ".mysqli_error($link));
                $classrooms = array();
                while ($r = mysqli_fetch_assoc($query)) $classrooms[$r["id"]] = $r["name"];
                mysqli_free_result($query);

	            // Get subjects
                $query = mysqli_query($link, "SELECT id, name FROM `".$db_prefix."subjects`") or die ("Error: (5.0.2) ".mysqli_error($link));
                $subjects = array();
                while ($r = mysqli_fetch_assoc($query)) $subjects[$r["id"]] = $r["name"];
                mysqli_free_result($query);

	            // Get people
                $query = mysqli_query($link, "SELECT id, name FROM `".$db_prefix."people`") or die ("Error: (5.0.3) ".mysqli_error($link));
                $people = array();
                while ($r = mysqli_fetch_assoc($query)) $people[$r["id"]] = $r["name"];
                mysqli_free_result($query);

	            // Get forms
                $query = mysqli_query($link, "SELECT id, name FROM `".$db_prefix."forms`") or die ("Error: (5.0.4) ".mysqli_error($link));
                $forms = array();
                while ($r = mysqli_fetch_assoc($query)) $forms[$r["id"]] = $r["name"];
                mysqli_free_result($query);

            	$eventfilter = "";
            	$schedulefilter = "";
            	$filter_name = "";
                $filter_type = mysqli_real_escape_string($link, $_GET['filter_type']); // form OR person
                if (!empty($filter_type)) {
	                $id = $_GET['id']; // id of the form OR person
	                if (!is_numeric($id)) die("Error: specified ID ".$id." isn't numeric");
	                if ($filter_type === "person") {
	                	$filter_name = $people[$id];
	                	$eventfilter = "`id` in (SELECT eventid FROM `".$db_prefix."events_people` WHERE `personid` = ".$id.")";
	                	$schedulefilter = "`teacherid` = ".$id;
	                } else if ($filter_type === "form") {
	                	$filter_name = $forms[$id];
	                	$eventfilter = "`id` in (SELECT eventid FROM `".$db_prefix."events_forms` WHERE `formid` = ".$id.")";
	                	$schedulefilter = "`id` IN (SELECT scheduleid FROM `".$db_prefix."periods_schedules_forms` WHERE `formid` = ".$id.")";
	                } else if ($filter_type === "subject") {
	                	$filter_name = $subjects[$id];
	                	$schedulefilter = "`subjectid` = ".$id;
	                } else if ($filter_type === "classroom") {
	                	$filter_name = $classrooms[$id];
	                	$schedulefilter = "`classroomid` = ".$id;
	                }
	            }

            	$result = "BEGIN:VCALENDAR\n"
            		."PRODID:-//Gregor Eesmaa/NONSGML ".$school_name." tunniplaan//ET\n"
            		."VERSION:2.0\n"
            		."CALSCALE:GREGORIAN\n"
            		."METHOD:PUBLISH\n"
            		."X-WR-CALNAME:".$school_name.": ".$filter_name."\n";

            	$mysql_dt_format = "Y-m-d H:i:s";
            	$mysql_d_format = "Y-m-d";
            	$schedule_time_format = "Y-m-d H:i";
            	$ical_dt_format = "Ymd\THis";
            	$ical_dt_format_z = "Ymd\THis\Z";
            	$ical_d_format = "Ymd";

            	$utc_timestamp = date($ical_dt_format_z);            	
            	$local_timezone = timezone_open($local_timezone_str);
            	$utc_timezone = timezone_open("UTC");

            	$result .= "BEGIN:VTIMEZONE\n"
            		."TZID:".$local_timezone_str."\n"
            		."X-LIC-LOCATION:".$local_timezone_str."\n"
            		."BEGIN:DAYLIGHT\n"
            		."TZOFFSETFROM:+0200\n"
            		."TZOFFSETTO:+0300\n"
            		."TZNAME:EEST\n"
            		."DTSTART:19700329T030000\n"
            		."RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU\n"
            		."END:DAYLIGHT\n"
            		."BEGIN:STANDARD\n"
            		."TZOFFSETFROM:+0300\n"
            		."TZOFFSETTO:+0200\n"
            		."TZNAME:EET\n"
            		."DTSTART:19701025T040000\n"
            		."RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU\n"
            		."END:STANDARD\n"
            		."END:VTIMEZONE\n";

	            // Add events
	            if (!empty($eventfilter)) {
	                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."events` WHERE ".$eventfilter." AND `end` >= DATE_SUB(NOW(), INTERVAL 60 DAY)") or die ("Error: (5.1) ".mysqli_error($link));
	                while ($r = mysqli_fetch_assoc($query)) {
	                	$utc_createdtime = date_format(date_timezone_set(date_create_from_format($mysql_dt_format, $r["created"], $local_timezone), $utc_timezone), $ical_dt_format_z);
	                    if (!empty($r["modified"])) {
	                    	$utc_modifiedtime = date_format(date_timezone_set(date_create_from_format($mysql_dt_format, $r["modified"], $local_timezone), $utc_timezone), $ical_dt_format_z);
	                    } else {
	                    	$utc_modifiedtime = "";
	                    }
	                    $result .= "BEGIN:VEVENT\n"
	                    	."UID:event-".$r['id']."@jpg.tartu.ee\n"
							."SUMMARY:".mysqli_real_escape_string($link, $r['name'])."\n"
							."LOCATION:".mysqli_real_escape_string($link, $r['location'])."\n"
							."DESCRIPTION:".mysqli_real_escape_string($link, $r['description'])."\n"
							."CREATED:".$utc_createdtime."\n"
							.(!empty($utc_modifiedtime) ? "LAST-MODIFIED:".$utc_modifiedtime."\n" : "")
							."DTSTAMP:".$utc_timestamp."\n";
						if ($r["allday"]) {							
		                    $startdate = date_format(date_create_from_format($mysql_dt_format, $r["start"]), $ical_d_format);
		                    $enddate = date_format(date_modify(date_create_from_format($mysql_dt_format, $r["end"]), "+1 day"), $ical_d_format);
							$result .= "DTSTART;VALUE=DATE:".$local_starttime."\n"
								."DTEND;VALUE=DATE:".$local_endtime."\n"
								."X-MICROSOFT-CDO-ALLDAYEVENT:TRUE\n";
						} else {
		                    $local_starttime = date_format(date_create_from_format($mysql_dt_format, $r["start"]), $ical_dt_format);
		                    $local_endtime = date_format(date_create_from_format($mysql_dt_format, $r["end"]), $ical_dt_format);
							$result .= "DTSTART;TZID=".$local_timezone_str.":".$local_starttime."\n"
								."DTEND;TZID=".$local_timezone_str.":".$local_endtime."\n"
								."X-MICROSOFT-CDO-ALLDAYEVENT:FALSE\n";
						}
	                    $result .= "END:VEVENT\n";
	                }
	                mysqli_free_result($query);
	            }

	            // Add holidays
                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."holidays` WHERE `end` >= DATE_SUB(NOW(), INTERVAL 60 DAY)") or die ("Error: (5.2) ".mysqli_error($link));
                $holidayranges = array();
                while ($r = mysqli_fetch_assoc($query)) {
                	$utc_modifiedtime = date_format(date_timezone_set(date_create_from_format($mysql_dt_format, $r["modified"], $local_timezone), $utc_timezone), $ical_dt_format_z);
                    $startdate = date_format(date_create_from_format($mysql_d_format, $r["start"]), $ical_d_format);
                    $enddate = date_format(date_modify(date_create_from_format($mysql_d_format, $r["end"]), "+1 day"), $ical_d_format); // end must be 1 day later
                    $result .= "BEGIN:VEVENT\n"
                    	."UID:holiday-".$r['id']."@jpg.tartu.ee\n"
						."SUMMARY:".mysqli_real_escape_string($link, $r['name'])."\n"
						."DESCRIPTION:\n"
						."LAST-MODIFIED:".$utc_modifiedtime."\n"
						."DTSTAMP:".$utc_timestamp."\n"
						."DTSTART;VALUE=DATE:".$startdate."\n"
						."DTEND;VALUE=DATE:".$enddate."\n"
						."X-MICROSOFT-CDO-ALLDAYEVENT:TRUE\n" // Make use of this when info added to database.
                    	."END:VEVENT\n";
                	$holidayranges[] = array("start" => strtotime($r["start"]), "end" => strtotime($r["end"]));
                }
                mysqli_free_result($query);

	            if (!empty($schedulefilter)) {
	                $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods` WHERE `end` >= DATE_SUB(NOW(), INTERVAL 60 DAY)") or die ("Error: (5.3) ".mysqli_error($link));
	                while ($r = mysqli_fetch_assoc($query)) {
	                	$utc_createdtime = date_format(date_timezone_set(date_create_from_format($mysql_dt_format, $r["created"], $local_timezone), $utc_timezone), $ical_dt_format_z);
	                    $utc_modifiedtime = date_format(date_timezone_set(date_create_from_format($mysql_dt_format, $r["last_modified"], $local_timezone), $utc_timezone), $ical_dt_format_z);
	                    $utc_periodenddate = date_format(date_modify(date_create_from_format($mysql_d_format, $r["end"]), "+1 day"), $ical_d_format); // end must be 1 day later

	                    // Attach lessonperiods
	                    $query2 = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_lessonperiods` WHERE `periodid` = '".$r["id"]."';") or die ("Error: (5.3.1 '".$r["id"]."') ".mysqli_error($link));
	                    $lessonperiods = array();
	                    while ($r2 = mysqli_fetch_assoc($query2)) {
	                        $lessonperiods[$r2["id"]] = $r2;
	                    }
	                    mysqli_free_result($query2);

	                    // Calculate first weekdays in period
	                	$startTime = strtotime($r["start"]);
						$weekEndTime = $startTime + 518400; // 6 days later
						$endTime = strtotime($r["end"]); 

						$firstWeekDate = array();
						$dayExdates = array();
						for ($i = $startTime; $i <= $endTime; $i += 86400) {
							$weekdayIdx = date("w", $i);
							// 0 = 7, because Sunday
							if ($weekdayIdx == 0) {
								$weekdayIdx = 7;
							}
							// I day within first week, set the first occurring date.
							if ($i <= $weekEndTime) {
								$firstWeekDate[$weekdayIdx] = date($mysql_d_format, $i);
								$dayExdates[$weekdayIdx] = array();
							}
							// Check if date falls in any holiday date ranges, if so, add it to exclusion list.
							foreach ($holidayranges as $holiday) {
								// Check if is on holiday
								if ($holiday["start"] <= $i && $i <= $holiday["end"]) {
									$dayExdates[$weekdayIdx][] = date($mysql_d_format, $i); 
								}
							}
						}

						$scheduleFormIDs = array();
	                    $query2 = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules_forms` WHERE `scheduleid` IN (SELECT id FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$r["id"]." AND ".$schedulefilter.")") or die ("Error: (5.3.2) ".mysqli_error($link));
	                    while ($r2 = mysqli_fetch_assoc($query2)) $scheduleFormIDs[$r2["scheduleid"]][] = &$r2["formid"];
	                    mysqli_free_result($query2);

	                    $query2 = mysqli_query($link, "SELECT * FROM `".$db_prefix."periods_schedules` WHERE `periodid` = ".$r["id"]." AND ".$schedulefilter) or die ("Error: (5.3.3 '".$r["id"]."') ".mysqli_error($link));
	                    while ($r2 = mysqli_fetch_assoc($query2)) {
	                    	$daystr = "";
	                    	switch($r2["weekday"]) {
	                    		case 1: $daystr = "MO"; break;
	                    		case 2: $daystr = "TU"; break;
	                    		case 3: $daystr = "WE"; break;
	                    		case 4: $daystr = "TH"; break;
	                    		case 5: $daystr = "FR"; break;
	                    		case 6: $daystr = "SA"; break;
	                    		case 7: $daystr = "SU"; break;
	                    	}
	                    	$local_starttime = date_format(date_create_from_format($schedule_time_format, $firstWeekDate[$r2["weekday"]]." ".$lessonperiods[$r2["lessonperiodid"]]["starttime"]), $ical_dt_format);
	                    	$local_endtime = date_format(date_create_from_format($schedule_time_format, $firstWeekDate[$r2["weekday"]]." ".$lessonperiods[$r2["lessonperiodid"]]["endtime"]), $ical_dt_format);
	                    	$local_exdates = "";
	                    	foreach($dayExdates[$r2["weekday"]] as $exday) {
	                    		if ($local_exdates !== "") {
	                    			$local_exdates.=",";
	                    		}
	                    		$local_exdates.= date_format(date_create_from_format($schedule_time_format, $exday." ".$lessonperiods[$r2["lessonperiodid"]]["starttime"]), $ical_dt_format);
	                    	}

	                    	$name = mysqli_real_escape_string($link, $subjects[$r2['subjectid']]);

	                		$formsstr = "";
	                		foreach ($scheduleFormIDs[$r2["id"]] as $formid) {
	                			if ($formsstr !== "") {
	                				$formsstr.=", ";
	                			}
	                			$formsstr.=$forms[$formid];
	                		}
	                    	$classroomstr = "";
	                    	if (!empty($r2['classroomid'])) {
	                    		$classroomstr = mysqli_real_escape_string($link, $classrooms[$r2['classroomid']]);
	                    	}
	                    	$specifier = "";
	                    	if ($filter_type !== "person") {
	                    		$specifier = $people[$r2["teacherid"]];
	                    	}
	                    	if ($filter_type !== "form") {
	                    		if (!empty($specifier))
	                    			$specifier.= "; ";
	                    		$specifier.=$formsstr;
	                    	}
	                    	if (!empty($specifier)) {
	                    		$name.= " (".$specifier.")";
	                    	}
	                    	$description = "Õpetaja: ".$people[$r2["teacherid"]];
	                    	$description.= "\\nOsalejad: ".$formsstr;
	                    	$description.= "\\nAsukoht: ".$classroomstr;
		                    $result .= "BEGIN:VEVENT\n"
		                    	."UID:schedule-".$r2['id']."@jpg.tartu.ee\n"
								."SUMMARY:".$name."\n"
								."DESCRIPTION:".$description."\n"
								."LOCATION:".$classroomstr."\n"
								."DTSTART;TZID=".$local_timezone_str.":".$local_starttime."\n"
								."DTEND;TZID=".$local_timezone_str.":".$local_endtime."\n"
								."RRULE:FREQ=WEEKLY;UNTIL=".$utc_periodenddate.";INTERVAL=1;BYDAY=".$daystr."\n"
								.(!empty($local_exdates) ? "EXDATE;TZID=".$local_timezone_str.":".$local_exdates."\n" : "")
								."CREATED:".$utc_createdtime."\n"
								."LAST-MODIFIED:".$utc_modifiedtime."\n"
								."DTSTAMP:".$utc_timestamp."\n"
		                    	."END:VEVENT\n";
	                    }
	                }
	            }

                $result .= "END:VCALENDAR";
                $response = $result;
            	break;
        }
        mysqli_close($link);
        if (isSet($response)) {
            if (!is_dir($cache_dir)) {
                mkdir($cache_dir, 0777, true);
            }
            file_put_contents($cache_file, $response);
            print $response;
        }
    }
?>