<?php
    require_once "./config.php";
    require_once "./verify.php";

	if ($level < 0) {
		// If user hasn't logged in, provide the UI with a login URL
		$result = array("level" => -1, "auth_url" => $stuudium_auth_url."?client_id=".$stuudium_client_id."&subdomain=".$stuudium_subdomain);
	} else {
		// If user has logged in, provide the UI with his data
		$query = mysqli_query($link, "SELECT * FROM `".$db_prefix."people` WHERE `id` = (SELECT personid FROM  `".$db_prefix."users` WHERE `userid` = '".$_SESSION["userid"]."')") or die ("Error: (0) ".mysqli_error($link));
		$result = mysqli_fetch_assoc($query);
		$result["level"] = $level;
		mysqli_free_result($query);
	}

	// Print out data in JSON format
	if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
	    $response = json_encode($result, JSON_UNESCAPED_UNICODE);
	} else {
		$response = json_encode($result);
	}
	echo $response;

	// Close any possible database connections
	if (isset($link))
		mysqli_close($link);
?>