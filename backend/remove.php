<?php
	/*
	* Provides safe methods for removing various data from the database.
	* Accepts POST or GET request with variables:
	*	datatype - "period", "holiday", "event", "group"
	*	id - numerical value (representing the ID of the data to be removed)
	* OUTPUT: "" (or "Error: errormsg")
	*/

	require_once "./config.php";
    require_once "./verify.php";
	connectDB();
	$datatype = $_POST['datatype'];
	switch ($datatype) {
		case "period":
			if ($level < 5)
				break;
			// TODO remove period (including period data)
			$periodid = $_POST['id'];
			if (!is_numeric($periodid)) {
				die("Error: id ".$periodid." isn't numeric");
			}

			// REMOVE PERIOD XML
			$xml_filename_query = mysqli_query($link, "SELECT filename FROM `".$db_prefix."periods` WHERE `id` = ".$periodid.";") or die ("Error: ".mysqli_error($link));
			while ($row = mysqli_fetch_row($xml_filename_query)) {
				$xml_name = $periodid."_".$row[0];
				$xmlfile = $timetable_xml_dir.$xml_name;
				if (file_exists($xmlfile)) {
					unlink($xmlfile);
				}
			}
			mysqli_free_result($xml_filename_query);

			// REMOVING PERIOD DATA
			mysqli_query($link, "DELETE FROM `".$db_prefix."periods` WHERE `id` = ".$periodid.";") or die ("Error: ".mysqli_error($link));

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{primary,range,filtered_".$periodid.",period_".$periodid.",export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true", "id" => $periodid);
			$response = json_encode($result);
			echo $response;
			break;
		case "holiday":
			if ($level < 5)
				break;
			$holidayid = $_POST['id'];
			if (!is_numeric($holidayid)) {
				die("Error: id ".$holidayid." isn't numeric");
			}

			// REMOVING HOLIDAY DATA
			mysqli_query($link, "DELETE FROM `".$db_prefix."holidays` WHERE `id` = ".$holidayid.";") or die ("Error: ".mysqli_error($link));

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{range,export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true", "id" => $holidayid);
			$response = json_encode($result);
			echo $response;
			break;
		case "event":
			if ($level < 1)
				break;
			$eventid = $_POST['id'];
			if (!is_numeric($eventid)) {
				die("Error: id ".$eventid." isn't numeric");
			}

			// REMOVING EVENT DATA
			mysqli_query($link, "DELETE FROM `".$db_prefix."events` WHERE `id` = ".$eventid.";") or die ("Error: ".mysqli_error($link));

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{primary,range,export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true", "id" => $eventid);
			$response = json_encode($result);
			echo $response;
			break;
		case "group":
			if ($level < 5)
				break;
			$groupid = $_POST['id'];
			if (!is_numeric($groupid)) {
				die("Error: id ".$groupid." isn't numeric");
			}

			// REMOVING GROUP DATA
			mysqli_query($link, "DELETE FROM `".$db_prefix."groups` WHERE `id` = ".$groupid.";") or die ("Error: ".mysqli_error($link));

			$result = array("success" => "true", "id" => $groupid);
			$response = json_encode($result);
			echo $response;
			break;
		case "cache":
			if ($level < 5)
				break;
			$cache_files = glob($cache_dir."_*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true");
			$response = json_encode($result);
			echo $response;
			break;
	}

	mysqli_close($link);
?>