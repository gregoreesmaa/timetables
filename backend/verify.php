<?php
	// Connect to database and start a fresh session
    require_once "./config.php";
    session_start();

	if (in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
    	$level = 100;    	
	    $_SESSION["userid"] = "0";
	    $_SESSION["token"] = "none";
		connectDB();
	} else if (!isset($_SESSION["token"]) || !isset($_SESSION["userid"])) {
    	// Token or userid isn't set, assuming the user is logged out.
    	session_unset();
    	$level = -1;
    } else {
    	// Verifying session data in the database
		connectDB();
	    $_SESSION["userid"] = mysqli_real_escape_string($link, $_SESSION["userid"]);
	    $_SESSION["token"] = mysqli_real_escape_string($link, $_SESSION["token"]);
	    // Update last visit time
	    $query = mysqli_query($link, "UPDATE `".$db_prefix."users_sessions` SET `last_visit`=CURRENT_TIMESTAMP WHERE `token` = '".$_SESSION["token"]."' AND `userid` = '".$_SESSION["userid"]."'") or die ("Error: (1v) ".mysqli_error($link));
	    $rescount = mysqli_affected_rows($link);
	    if ($rescount == 0) {
	    	// Token no longer exists, assuming the user is logged out.
	    	session_unset();
	    	$level = -1;
	    } else {
	    	// Fetching user data for the user
		    $query = mysqli_query($link, "SELECT * FROM `".$db_prefix."users` WHERE `userid` = '".$_SESSION["userid"]."'") or die ("Error: (2v) ".mysqli_error($link));;
		    $result = mysqli_fetch_assoc($query);
		    $level = $result["level"];
	    	mysqli_free_result($query);
		}
	}
?>