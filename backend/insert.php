<?php
	/*
	* Inserts data to database. To keep the code shorter, this script uses sub-scripts to do its job.
	*/
    require_once "./config.php";
    require_once "./verify.php";
	connectDB();
	$datatype = $_POST['datatype'];
	switch ($datatype) {
		case "event":
			if ($level < 1)
				break;
			/*
			* Inserts or updates event into database.
			* Receives REQUEST request with variables: 
			*	create - 0 (update) or 1 (create)
			*	name - text up to 48 characters (name of the event)
			*	description - text up to 65535 characters (longer description of the event)
			*	location - text up to 48 characters (location of the event)
			*   classroomid - classroom ID
			*   forms - JSON encoded array of participating form IDs
			*   people - JSON encoded array of participating peoples' IDs
			*	start - YYYY-MM-DD HH:MM:SS format (start time of the event, inclusive)
			*	end - YYYY-MM-DD HH:MM:SS format (end time of the event, inclusive)
			*	allday - 0 for false, 1 for true
			*   forall - 0 for false, 1 for true
			* 	color - #XXXXXX (HEX representation of the event color)
			*	eventid (only if create == 0 aka update) - numerical value (representing the ID of the event to be updated)
			* OUTPUT: "eventid" (or "Error: errormsg")
			*	eventid - numerical value (ID of the added event)
			*/

			$create = $_POST['create'] == 1;
			$name = mysqli_real_escape_string($link, mb_convert_encoding($_POST['name'], "UTF-8"));
			if (isset($_POST['description'])) {
				$description = "'".mysqli_real_escape_string($link, mb_convert_encoding($_POST['description'], "UTF-8"))."'";
			} else {
				$description = "NULL";
			}
			if (isset($_POST['location'])) {
				$location = "'".mysqli_real_escape_string($link, mb_convert_encoding($_POST['location'], "UTF-8"))."'";
			} else {
				$location = "NULL";
			}
			if (isset($_POST['classroomid'])) {
				$classroomid = "'".mysqli_real_escape_string($link, mb_convert_encoding($_POST['classroomid'], "UTF-8"))."'";
			} else {
				$classroomid = "NULL";
			}
			$forms = json_decode($_POST['forms']);
			$formsCount = count($forms);
			for ($i = 0; $i < $formsCount; $i++) {
				$forms[$i] = mysqli_real_escape_string($link, $forms[$i]);
			}
			$people = json_decode($_POST['people']);		
			$peopleCount = count($people);
			for ($i = 0; $i < $peopleCount; $i++) {
				$people[$i] = mysqli_real_escape_string($link, $people[$i]);
			}
			$start = mysqli_real_escape_string($link, $_POST['start']);
			$end = mysqli_real_escape_string($link, $_POST['end']);
			$forall = $_POST['forall'] == 0 ? 0 : 1;
			$allday = $_POST['allday'] == 0 ? 0 : 1;
			$color = hexdec(substr($_POST['color'], 1));
			if ($create) {
				$creatorid = $_SESSION["userid"];
				if (!is_numeric($creatorid)) die("Error: creatorid ".$creatorid." isn't numeric");
				mysqli_query($link, "INSERT INTO `".$db_prefix."events` (`id`, `name`, `description`, `location`, `classroomid`, `color`, `start`, `end`, `allday`, `forall`, `creatorid`, `created`, `modifierid`, `modified`) VALUES (NULL, '".$name."', ".$description.", ".$location.", ".$classroomid.", '".$color."', '".$start."', '".$end."', '".$allday."', '".$forall."', '".$creatorid."', CURRENT_TIMESTAMP, NULL, NULL)") or die ("Error: (1.1) ".mysqli_error($link));
				$eventid = $link->insert_id;				
				$forms_query = "";
				$people_query = "";
				foreach ($forms as $formid) $forms_query .= "('".$formid."', '".$eventid."'), ";
				foreach ($people as $personid) $people_query .= "('".$personid."', '".$eventid."'), ";
				if (!empty($forms_query)) mysqli_query($link, "INSERT INTO `".$db_prefix."events_forms` (`formid`, `eventid`) VALUES ".rtrim($forms_query, ", ")) or die ("Error: (1.2) ".mysqli_error($link));	
				if (!empty($people_query)) mysqli_query($link, "INSERT INTO `".$db_prefix."events_people` (`personid`, `eventid`) VALUES ".rtrim($forms_query, ", ")) or die ("Error: (1.3) ".mysqli_error($link));
			} else {
				$modifierid = $_SESSION["userid"];
				$eventid = $_POST['eventid'];
				if (!is_numeric($modifierid)) die("Error: modifierid ".$modifierid." isn't numeric");
				else if (!is_numeric($eventid)) die("Error: eventid ".$eventid." isn't numeric");
				mysqli_query($link, "UPDATE `".$db_prefix."events` SET `name` = '".$name."', `description` = ".$description.", `location` = ".$location.", `classroomid` = ".$classroomid.", `color` = '".$color."', `start` = '".$start."', `end` = '".$end."', `forall` = '".$forall."', `allday` = '".$allday."', `modifierid` = '".$modifierid."' WHERE `id` = ".$eventid.";") or die ("Error: (1.4) ".mysqli_error($link));
				// TODO add removal of participants
			}

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{primary,range,export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true", "id" => $eventid);
			$response = json_encode($result);
			echo $response;
			break;
		case "holiday":
			if ($level < 5)
				break;	
			/*
			* Inserts or updates holiday into database.
			* Receives REQUEST request with variables: 
			*	create - 0 (update) or 1 (create)
			*	name - text up to 48 characters (name of the holiday)
			*	description - text up to 65535 characters (longer description of the event)
			*	start - YYYY-MM-DD format (start date of the holiday, inclusive)
			*	end - YYYY-MM-DD format (end date of the holiday, inclusive)
			*	holidayid (only if create == 0 aka update) - numerical value (representing the ID of the holiday to be updated)
			* OUTPUT: "holidayid" (or "Error: errormsg")
			*	holidayid - numerical value (ID of the added holiday)
			*/

			$create = $_POST['create'] == 1;
			$name = mysqli_real_escape_string($link, mb_convert_encoding($_POST['name'], "UTF-8"));
			$description = mysqli_real_escape_string($link, mb_convert_encoding($_POST['description'], "UTF-8"));
			$start = mysqli_real_escape_string($link, $_POST['start']);
			$end = mysqli_real_escape_string($link, $_POST['end']);

			if ($create) {
				mysqli_query($link, "INSERT INTO `".$db_prefix."holidays` (`id`, `name`, `description`, `start`, `end`, `modified`) VALUES (NULL, '".$name."', '".$description."', '".$start."', '".$end."', CURRENT_TIMESTAMP);") or die ("Error: (2.1) ".mysqli_error($link));
				$holidayid = $link->insert_id;
			} else {
				$holidayid = $_POST['holidayid'];
				if (!is_numeric($holidayid)) die("Error: holidayid ".$holidayid." isn't numeric");
				mysqli_query($link, "UPDATE `".$db_prefix."holidays` SET `name` = '".$name."', `description` = '".$description."', `start` = '".$start."', `end` = '".$end."' WHERE `id` = ".$holidayid.";") or die ("Error: (2.2) ".mysqli_error($link));
			}

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{range,export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true", "id" => $holidayid);
			$response = json_encode($result);
			echo $response;
			break;
		case "override":
			// Add levelcheck
			/*
			* Inserts or updates holiday into database.
			* Receives REQUEST request with variables: 
			*   scheduleid - number representing the ID of the schedule
			*	weekday - number from 1 to 5 representing the day from monday to friday respectively
			*	lessonperiodid - number representing the lessonperiod of the schedule
			*	classroomid - number representing the id of the classroom
			*	teacherid - number representing the id of the teacher
			*	startweek - the starting week in YYYY-MM-DD format
			*   countinuous - 1 if the override is continuous, 0 if it is not
			*   cancelled - 1 if the schedule is cancelled, 0 if it is not
			* OUTPUT: "holidayid" (or "Error: errormsg")
			*	holidayid - numerical value (ID of the added holiday)
			*/

			$scheduleid = mysqli_real_escape_string($link, $_POST['scheduleid']);
			if (isset($_POST['weekday'])) {
				$weekday = "'".mysqli_real_escape_string($link, $_POST['weekday'])."'";
			} else {
				$weekday = "NULL";
			}
			if (isset($_POST['lessonperiodid'])) {
				$lessonperiodid = "'".mysqli_real_escape_string($link, $_POST['lessonperiodid'])."'";
			} else {
				$lessonperiodid = "NULL";
			}
			if (isset($_POST['classroomid'])) {
				$classroomid = "'".mysqli_real_escape_string($link, $_POST['classroomid'])."'";
			} else {
				$classroomid = "NULL";
			}
			if (isset($_POST['teacherid'])) {
				$teacherid = "'".mysqli_real_escape_string($link, $_POST['teacherid'])."'";
			} else {
				$teacherid = "NULL";
			}
			$forms = json_decode($_POST['forms']);
			$formsCount = count($forms);
			for ($i = 0; $i < $formsCount; $i++) {
				$forms[$i] = mysqli_real_escape_string($link, $forms[$i]);
			}
			$startweek = mysqli_real_escape_string($link, $_POST['startweek']);
			$continuous = mysqli_real_escape_string($link, $_POST['continuous']);
			$cancelled = mysqli_real_escape_string($link, $_POST['cancelled']);

			mysqli_query($link, "INSERT INTO `".$db_prefix."periods_schedules_override` (`id`, `scheduleid`, `weekday`, `lessonperiodid`, `classroomid`, `teacherid`, `startweek`, `continuous`, `cancelled`) VALUES (NULL, '".$scheduleid."', ".$weekday.", ".$lessonperiodid.", ".$classroomid.", ".$teacherid.", '".$startweek."', '".$continuous."', '".$cancelled."') ON DUPLICATE KEY UPDATE `weekday` = ".$weekday.", `lessonperiodid` = ".$lessonperiodid.", `classroomid` = ".$classroomid.", `teacherid` = ".$teacherid.", `continuous` = '".$continuous."', `cancelled` = '".$cancelled."';") or die ("Error: (4.1) ".mysqli_error($link));
			$overrideid = $link->insert_id;
			// Row already exists, get ID
			if ($overrideid === 0) {
				$overrideidQuery = mysqli_query($link, "SELECT `id` FROM `".$db_prefix."periods_schedules_override` WHERE `scheduleid`='".$scheduleid."' AND `startweek`='".$startweek."'") or die ("Error: (4.1.2) ".mysqli_error($link));
				$overrideid = mysqli_fetch_array($overrideidQuery)[0];
				mysqli_free_result($overrideidQuery);
			}

			/* 
			DELETE FROM `tt_periods_schedules_forms` WHERE `tt_periods_schedules_forms`.`formid` = 1 AND `tt_periods_schedules_forms`.`scheduleid` = 42 
			*/

			mysqli_query($link, "DELETE FROM `".$db_prefix."periods_schedules_override_forms` WHERE `overrideid` = '".$overrideid."'") or die ("Error: (4.2) ".mysqli_error($link));

    		$forms_query = "";
			foreach ($forms as $formid) $forms_query .= "('".$formid."', '".$overrideid."'), ";
			if (!empty($forms_query)) {
		        mysqli_query($link, "INSERT INTO `".$db_prefix."periods_schedules_override_forms` (`formid`, `overrideid`) VALUES ".rtrim($forms_query, ", ")) or die ("Error: (4.3) ".mysqli_error($link));
		    }

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*{primary,filtered,export-ical}*", GLOB_BRACE);
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}

			$result = array("success" => "true");
			$response = json_encode($result);
			echo $response;
			break;
		case "timetable":
			if ($level < 5)
				break;	
			require "./insertTimetable.php";

			// delete cached files which are affected by this change
			$cache_files = glob($cache_dir."_*");
			foreach ($cache_files as $cache_file) {
				if (is_file($cache_file))
					unlink($cache_file);
			}
			break;
	}
	mysqli_close($link);
?>