<?php
	// Connect with the database and start a fresh session
    require_once "./config.php";    
	connectDB();
    session_start();

    // Receive verification data and verify it
	$token = mysqli_real_escape_string($link, $_GET["token"]);
	$signature = hash_hmac('sha1', $token, $stuudium_client_secret);
	$requestURL = "https://api.ope.ee/v1/authenticate/verify?token=".$token."&client_id=".$stuudium_client_id."&signature=".$signature;
	$ch = curl_init($requestURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	$userdata = json_decode($result, true);

	// If error
	if ($userdata["error"]) {
		die("Error: Login failed: ".$userdata["message"]);
	}

	// Add new user and person, if didn't exist before
	$userid = mysqli_real_escape_string($link, $userdata["id"]);
	$fullname = mysqli_real_escape_string($link, $userdata["name_first"]." ".$userdata["name_last"]);
	$alias = mysqli_real_escape_string($link, preg_replace('/[^\p{L}\d]/u', '', mb_strtolower($userdata["name_first"]." ".$userdata["name_last"], "UTF-8")));
	$user_query = mysqli_query($link, "SELECT * FROM `".$db_prefix."users` WHERE `userid` = '".$userid."'") or die ("Error: (1) ".mysqli_error($link));
	if (mysqli_num_rows($user_query) === 0) {
		$person_query = mysqli_query($link, "SELECT * FROM `".$db_prefix."people` WHERE `alias` = '".$alias."'") or die ("Error: (2) ".mysqli_error($link));
		if (mysqli_num_rows($person_query) === 0) {
			mysqli_query($link, "INSERT INTO `".$db_prefix."people` (`id`, `alias`, `name`) VALUES (NULL, '".$alias."', '".$fullname."')") or die ("Error: (3) ".mysqli_error($link));
			$personid = $link->insert_id;				
		} else {
			$person_row = mysql_fetch_assoc($person_query);
			$personid = $person_row["id"];
		}
		mysqli_free_result($person_query);
		mysqli_query($link, "INSERT INTO `".$db_prefix."users` (`userid`, `personid`) VALUES ('".$userid."', '".$personid."')") or die ("Error: (4) ".mysqli_error($link));
	} else {
		$row = mysqli_fetch_assoc($user_query);
		$personid = $row["personid"];
	}
	mysqli_free_result($user_query);

	// Add auth data to database
    $token = bin2hex(openssl_random_pseudo_bytes(16));
    mysqli_query($link, "INSERT INTO `".$db_prefix."users_sessions` (`token`, `userid`, `created`) VALUES ('".$token."', '".$userid."', CURRENT_TIMESTAMP);") or die ("Error: (5) ".mysqli_error($link));
    $_SESSION["userid"] = $userid;
    $_SESSION["token"] = $token;	
	mysqli_close($link);

	// Redirect user to main page
    $host = $_SERVER['HTTP_HOST'];
	$uri = rtrim(dirname(dirname($_SERVER['PHP_SELF'])), '/\\');
	echo "http://".$host.$uri."/";
	header("Location: http://".$host.$uri."/");
	exit();
?>