(function() {
    angular
        .module("TimetableApp")
        .controller("PrintSettingsController", ['$http', '$routeParams', '$mdToast', 'upper', PrintController]);

    function PrintController($http, $routeParams, $mdToast, upper){
        var self = this;
        this.requesting = 0;

        // Filter data
        this.filter = {name: "...", type: "", id: 0};

        // Primary data
        this.forms = {};
        this.people = {};
        this.subjects = {};
        this.classrooms = {};

        // Period data
        this.period = {};
        this.schedules = {'form': [], 'person': [], 'classroom': [], 'subject': []};
        this.weekdays = moment.localeData()._weekdays;

        upper.backgroundColor = "#FFFFFF";

        // Initialize (ran after declaration of all methods)
        this.init = function() {
            self.requestPrimaryData();
            self.requestPeriodData($routeParams.periodId);

            if ($routeParams.formId) {
                self.filter.type = "form";
                self.filter.id = $routeParams.formId;
            } else if ($routeParams.personId) {
                self.filter.type = "person";
                self.filter.id = $routeParams.personId;
            } else if ($routeParams.classroomId) {
                self.filter.type = "classroom";
                self.filter.id = $routeParams.classroomId;
            } else if ($routeParams.subjectId) {
                self.filter.subject = "subject";
                self.filter.id = $routeParams.subjectId;
            }
            self.requestFilteredData($routeParams.periodId, self.filter);
        };

        this.openPrintDialog = function() {
            window.print();
        }

        this.getFormsStr = function(schedule) {
            if (!schedule.formsstr) {
                if (schedule.formids.length === Object.keys(self.forms).length) {
                    schedule.formsstr = "Kõik klassid";
                } else {
                    // Put all participating forms' names to an array
                    var formstrs = [];
                    for (formidx in schedule.formids) {
                        formstrs.push(self.forms[schedule.formids[formidx]].name);
                    }

                    // Sort the array
                    // TODO breaks when string sorting isn't identical to form integer sorting (eg. 9th grade and 10th grade)
                    formstrs.sort();

                    // Write all forms to a string using the format: 10.abcde, 11.abcde, 12.abcde
                    var formsstr = "";
                    var lastGrade = "";
                    for (formstr in formstrs) {
                        var grade = formstrs[formstr].match(/\d+\.*/)[0];
                        if (lastGrade !== grade) {
                            if (formsstr !== '') {
                                formsstr += ', ';
                            }
                            formsstr += grade;
                            lastGrade = grade;
                        }
                        var parallel = formstrs[formstr].replace(grade, '');
                        formsstr += parallel;
                    }
                    schedule.formsstr = formsstr;
                }
            }
            return schedule.formsstr;
        };

        this.getBlockStyle = function(schedule) {
            if (!schedule.teacherid || !self.people[schedule.teacherid]) {
                return {'background-color': '#FFFFFF', 'color': '#000000'};
            }
            if (!self.people[schedule.teacherid].blockStyle) {
                var bgcolorhex = self.people[schedule.teacherid].color;

                // Figure out text color on background based on it's contrast
                var splitcolor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(bgcolorhex);
                var bgcolor = splitcolor ? {
                    r: parseInt(splitcolor[1], 16),
                    g: parseInt(splitcolor[2], 16),
                    b: parseInt(splitcolor[3], 16)
                } : null;
                // Formula from http://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
                var textcontrast = (bgcolor.r * 0.299 + bgcolor.g * 0.587 + bgcolor.b * 0.114);
                if (textcontrast <= 120) {
                    self.people[schedule.teacherid].brightBg = true;
                }

                self.people[schedule.teacherid].blockStyle = {'background-color': bgcolorhex};
            }
            return self.people[schedule.teacherid].blockStyle;
        };

        this.toggleBlock = function($event) {
            var block = $event.srcElement;
            while ((' ' + block.className + ' ').indexOf(" table-schedule-container ") == -1) {
                block = block.parentElement;
            }
            if (block.getAttribute("print-mode") === "fade") {
                block.removeAttribute("print-mode");
            } else if (block.hasAttribute("print-mode")) {
                block.setAttribute("print-mode", "fade");
            } else {
                block.setAttribute("print-mode", "highlight");
            }
        }

        // Requesting primary data
        this.requestPrimaryData = function() {
            console.log('Requesting primary data');
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q: 'primary'}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error requesting primary data: ', response);
                    return;
                }
                self.forms = data.forms;
                self.people = data.people;
                self.subjects = data.subjects;
                self.classrooms = data.classrooms;

                // Initialize filter
                if ($routeParams.formId) {
                    self.filter.name = data.forms[$routeParams.formId].name;
                } else if ($routeParams.personId) {
                    self.filter.name = data.people[$routeParams.personId].name;
                } else if ($routeParams.classroomId) {
                    self.filter.name = data.classrooms[$routeParams.classroomId].name;
                } else if ($routeParams.subjectId) {
                    self.filter.name = data.subjects[$routeParams.subjectId].name;
                }
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error requesting primary data: ', error);
            });
        };

        // Requesting data in specified time range
        this.requestPeriodData = function(periodid) {
            console.log("Requesting period data for period", periodid);
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q: 'period', periodid: periodid}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == "Error:") {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Error.")
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error("Error requesting period data: ", data);
                    return;
                }
                data.start = moment(data.start, "YYYY-MM-DD");
                data.end = moment(data.end, "YYYY-MM-DD");
                data.startstr = data.start.format("DD. MMM YYYY");
                data.endstr = data.end.format("DD. MMM YYYY");
                self.period = data;
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent("Error " + error.status + ": " + error.statusText + " - " + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error("Error requesting period data: ", error);
            });
        };

        // Requesting data for specified filter and specified period
        var requestingFilteredDataFor = {};
        this.requestFilteredData = function(periodid, filter) {
            var filteruid = filter.type + "-" + filter.id;
            if (!requestingFilteredDataFor[periodid]) {
                requestingFilteredDataFor[periodid] = {};
                requestingFilteredDataFor[periodid][filteruid] = true;
            } else if (!requestingFilteredDataFor[periodid][filteruid]) {
                requestingFilteredDataFor[periodid][filteruid] = true;
            } else {
                console.log("Canceled filtered data request, because request has already been sent.");
                return;
            }

            console.log("Requesting filtered data for period", periodid, "with filter", filter);
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q: 'filtered', periodid: periodid, filter_type: filter.type, id: filter.id}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == "Error:") {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent("Error.")
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error("Error requesting filtered data:", data);
                    requestingFilteredDataFor[periodid][filteruid] = false;
                    return;
                }
                self.schedules[filter.type][filter.id] = data;
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent("Error " + error.status + ": " + error.statusText + " - " + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error("Error requesting filtered data:", error);
                requestingFilteredDataFor[periodid][filteruid] = false;
            });
        };

        this.init();
    }
})();