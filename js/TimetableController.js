(function() {
    angular
        .module('TimetableApp')
        .controller('TimetableController', ['$route', '$rootScope', '$scope', '$window', '$location', '$http', '$routeParams', '$filter', '$timeout', '$mdToast', 'upper', '$mdMedia', '$mdDialog', '$mdSidenav', TimetableController]);

    function TimetableController($route, $rootScope, $scope, $window, $location, $http, $routeParams, $filter, $timeout, $mdToast, upper, $mdMedia, $mdDialog, $mdSidenav){
        var self = this;

        this.requesting = 0;

        // Filter data
        this.range = {start: false, end: false};
        this.filter = {name: '...', type: '', id: 0};

        // Primary data
        this.forms = {};
        this.formsArr = [];
        this.publicFormsArr = [];
        this.people = {};
        this.peopleArr = [];
        this.publicPeopleArr = [];
        this.subjects = {};
        this.subjectsArr = [];
        this.publicSubjectsArr = [];
        this.classrooms = {};
        this.classroomsArr = [];
        this.publicClassroomsArr = [];
        this.filtersArray = [];
        this.publicFiltersArray = [];

        this.filterNormalLength = 0;

        // Range data
        this.periods = [];
        this.lessonperiods = [];
        this.weeks = {};
        this.currWeek = null;

        // Functions
        this.highlightLesson = false;
        this.highlightBreakBeforeLesson = false;

        this.user = null;
        this.auth_url = null;

        this.toggleFilterMenu = function(){
            return $mdSidenav('left').toggle();
        };

        this.isFilterMenuOpen = function(){
            return $mdSidenav('left').isOpen();
        };

        this.filterSearchForms = function(query) {
            if (!query)
                return [];
            var qr = query.toLowerCase();
            return $filter('filter')(self.formsArr, function(obj, idx, arr) {
                return obj.name.toLowerCase().indexOf(qr) > -1;
            });
        };

        this.filterSearchParticipants = function(query) {
            if (!query)
                return [];
            var qr = query.toLowerCase();
            return $filter('filter')(self.filtersArray, function(obj, idx, arr) {
                return obj.name.toLowerCase().indexOf(qr) > -1 && (obj.type === "form" || obj.type === "person");
            });
        };

        this.restoreOriginal = function(dataschedule, schedule) {
            schedule.changedate = true;
            schedule.changeclassroom = true;
            schedule.changeteacher = true;
            schedule.weekday = dataschedule.originalWeekday;
            schedule.lessonperiodid = dataschedule.originalLessonperiodid;
            if (dataschedule.originalClassroomid) {
                schedule.classroomid = dataschedule.originalClassroomid;
            } else {
                schedule.classroomid = -1;
            }
            schedule.teacherid = dataschedule.originalTeacherid;
            schedule.participants = self.listParticipants(dataschedule.originalFormids);
        };

        this.reloadTimetableData = function() {
            self.forms = {};
            self.formsArr = [];
            self.publicFormsArr = [];
            self.people = {};
            self.peopleArr = [];
            self.publicPeopleArr = [];
            self.subjects = {}
            self.subjectsArr = [];
            self.publicSubjectsArr = [];
            self.classrooms = {};
            self.classroomsArr = [];
            self.publicClassroomsArr = [];
            self.filtersArray = [];
            self.publicFiltersArray = [];
            self.periods = [];
            self.lessonperiods = [];
            self.weeks = {};
            self.currentWeek = null;
            requestingFilteredDataFor = {};
            self.requestPrimaryData();
            self.shiftDateRange(0);     
        };

        // Initialize (ran after declaration of all methods)
        this.init = function() {
            self.requestUserData();
            self.range.start = moment().startOf('week'); // time of monday
            self.range.end = moment().endOf('week'); // time of sunday
            self.reloadTimetableData();
        };

        this.openLoginPage = function() {
            $window.location.href = self.auth_url;
        };

        this.openPrintPage = function(periodid) {
            if (!periodid) {
                periodid = self.currWeek.allperiods[Object.keys(self.currWeek.allperiods)[0]].id;
            }
            window.open('#/print/period/' + periodid + '/' + self.filter.type + '/' + self.filter.id, '', 'width=1000, height=700');
        };

        this.showExportToast = function() {
            var export_url = document.location.origin + document.location.pathname + 'backend/request.php?q=export-ical&filter_type=' + self.filter.type + '&id=' + self.filter.id;
            $mdToast.show({
                  template: '<md-toast><div>URL:<input flex type=\'text\' value=\'' + export_url + '\' onClick=\'this.setSelectionRange(0, this.value.length)\' /><br><span class=\'md-caption\'>Selle lingi saad näiteks Google Calender\'ile anda.</span></div></md-toast>',
                  hideDelay: 6000,
                  position: 'top right'
            });
        }

        // Allow for keyboard navigation
        upper.keyDown = function(e) {
            // If filter search field is focused or dialog is open, we don't want to interpret the keycodes          
            if (self.dialogOpen) {
                return false;
            } else if (e.keyCode == 8 && !self.focusSearch) {
                if (self.isFilterMenuOpen()) {
                    self.focusSearch = '\b';
                }
                e.preventDefault();   
            } else if (self.isFilterMenuOpen()) {
                return false;
            } else if (e.keyCode == 37) { 
                // On left arrow press
                self.shiftDateRange(-7);
            } else if (e.keyCode == 39) { 
                // On right arrow press
                self.shiftDateRange(7);
            }
        };

        upper.keyPress = function(e) {
            if (self.dialogOpen) {
                return false;
            } else if (e.keyCode === 13 && self.isFilterMenuOpen()) {
                // Find the first item visible for the user and apply the filter
                var filter = $filter('filter')(self.filtersArray, self.filterSearch)[0];
                if (filter) {
                    self.setFilter(filter.type, filter.id, filter.name);
                } else {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Vasteid ei leitud.')
                            .position('top right')
                            .hideDelay(3000)
                    );  
                    if (self.filterSearch)
                        self.filterSearch.name = '';
                    self.toggleFilterMenu();
                }
            } else if (e.charCode != 0 && !self.focusSearch){
                if (!self.isFilterMenuOpen()) {
                    self.toggleFilterMenu();
                }
                self.focusSearch = String.fromCharCode(e.charCode);
            }
        }

        // Setting filter for schedules
        this.setFilter = function(type, id, name) {
            // Change browser URL without refreshing
            $location.path('/' + type + '/' + id, false);
            delete $routeParams[self.filter.type + 'Id'];
            $routeParams[type + 'Id'] = id;

            self.filter = {type: type, id: id, name: name};

            for (var periodid in self.currWeek.allperiods) {
                // Request filtered data if previously not requested yet
                if (!self.currWeek.allperiods[periodid].schedules[self.filter.type][self.filter.id]) {
                    self.requestFilteredData(periodid, self.filter, self.range.start.clone());
                }
            }
            self.filterEvents(self.range.start);
            self.filterSchedules(self.range.start);

            // Clearing the search value
            if (self.filterSearch) 
                self.filterSearch.name = '';
            if (self.isFilterMenuOpen())
                self.toggleFilterMenu();
        };

        // Shifting date range by specified amount of days
        this.shiftDateRange = function(days) {
            console.log('Shifting date range by', days, 'days');

            self.range.start.add(days, 'days');
            self.range.start.formattedStr = self.range.start.format('DD. MMM YYYY');
            self.range.start.formattedDay = self.range.start.format('DD');
            self.range.start.formattedMonth = self.range.start.format('MMM');
            var start = self.range.start.clone();

            self.range.end.add(days, 'days');
            self.range.end.formattedStr = self.range.end.format('DD. MMM YYYY');
            self.range.end.formattedDay = self.range.end.format('DD');
            self.range.end.formattedMonth = self.range.end.format('MMM');
            var end = self.range.end.clone();

            if (!self.weeks[start]) {
                // DBUG console.log("Creating week", start.format("YYYY-MM-DD"));
                self.weeks[start] = {allperiods: [], periodcount: 0, periods: [], events: [], holidays: [], dates: [], today: -1, eventsFilter: self.filter, schedulesFilter: self.filter, filteredEvents: false, filteredSchedules: false, weekendEvents: []};
                self.currWeek = self.weeks[start];
                for (var m = start.clone(); !m.isAfter(self.range.end); m.add(1, 'days')) {
                    var weekday = m.format('d');
                    self.weeks[start].dates[weekday] = m.clone();
                    self.weeks[start].dates[weekday].formattedStr = self.weeks[start].dates[weekday].format('dddd (DD.MM)');
                }
                self.recalculateCurrentDay();
                self.requestRangeData(start, end);
            } else {
                self.currWeek = self.weeks[start];
                for (var periodid in self.currWeek.allperiods) {
                    // Request filtered data if previously not requested yet
                    if (!self.currWeek.allperiods[periodid].schedules[self.filter.type][self.filter.id]) {
                        self.requestFilteredData(periodid, self.filter, start);
                    } else {
                        // DBUG console.log("Adding preadded week", start.format('YYYY-MM-DD'));
                    }
                }
                self.filterEvents(start);
                self.filterSchedules(start);
                self.setNextUpdateTimer();
            }
        };

        this.recalculateCurrentDay = function() {
            var today = moment();
            self.currWeek.today = -1;
            for (var m = self.range.start.clone(); !m.isAfter(self.range.end); m.add(1, 'days')) {
                var weekday = m.format('d');
                if (m.isSame(today, 'day')) {
                    self.currWeek.today = weekday;
                    break;
                }
            }
        };

        this.filterSchedules = function(weekstart) {
            if (!self.weeks[weekstart].filteredSchedules || self.filter != self.weeks[weekstart].schedulesFilter) {
                // DBUG console.log("Filtering schedules for", weekstart.format("YYYY-MM-DD"));
                self.weeks[weekstart].schedulesFilter = self.filter;
                self.weeks[weekstart].filteredSchedules = [];
                // Iterate through all days of the week 

                for (var weekday = 0; weekday < 7; weekday++) {
                    var dayperiod = self.weeks[weekstart].periods[weekday]; 
                    // DBUG console.log("\tA1");
                    if (!dayperiod) continue; // self.weeks[weekstart].filteredSchedules = false;
                    // DBUG console.log("\tB1");
                    self.weeks[weekstart].filteredSchedules[weekday] = [];
                    for (var lessonperiodid in dayperiod.lessonperiods) {
                        // Preparing arrays for overrides
                        self.weeks[weekstart].filteredSchedules[weekday][lessonperiodid] = [];
                    }
                }
                for (var weekday = 0; weekday < 7; weekday++) {
                    var dayperiod = self.weeks[weekstart].periods[weekday]; 
                    // DBUG console.log("\tA");
                    if (!dayperiod) continue; // self.weeks[weekstart].filteredSchedules = false;
                    // DBUG console.log("\tB");
                    if (self.filter.id === 0) continue;

                    //tc.currWeek.periods[weekday].schedules[tc.filter.type][tc.filter.id]
                    var weekschedules = dayperiod.schedules[self.filter.type][self.filter.id];
                    // DBUG console.log("\tC");
                    if (!weekschedules) continue;
                    var dayschedules = weekschedules[weekday];
                    // DBUG console.log("\tD");
                    if (!dayschedules) continue;
                    // Initialize a new schedules array
                    // Iterate through every lessonperiod
                    // DBUG console.log("\tAmount of dayschedules", Object.keys(dayschedules).length, "and dayperiods", Object.keys(dayperiod.lessonperiods).length);
                    for (var lessonperiodid in dayperiod.lessonperiods) {
                        var lessonperiodschedules = dayschedules[lessonperiodid];
                        if (!lessonperiodschedules) continue;
                        for (var i = 0; i < lessonperiodschedules.length; i++) {
                            var schedule = lessonperiodschedules[i];
                            // Find override
                            var filterMatch = true;

                            schedule.originalWeekday = schedule.weekday;
                            schedule.originalLessonperiodid = schedule.lessonperiodid;
                            schedule.originalClassroomid = schedule.classroomid;
                            schedule.originalTeacherid = schedule.teacherid;
                            schedule.originalFormids = schedule.formids;

                            if (schedule.overrides && Object.keys(schedule.overrides).length > 0) {
                                schedule = _.cloneDeep(schedule);
                                for (var overrideid in schedule.overrides) {
                                    var override = schedule.overrides[overrideid];
                                    if ((override.continuous == 1 && self.weeks[weekstart].dates[1].isSameOrAfter(override.startweek, "week"))
                                        || (override.continuous == 0 && self.weeks[weekstart].dates[1].isSame(override.startweek, "week"))) {

                                        // Check if override applied at later week. If yes, ignore this one.
                                        if (schedule.overriddenSince && moment(schedule.overriddenSince).isSameOrAfter(override.startweek, "week")) continue;

                                        console.log("Overriding", override, self.weeks[weekstart].dates[1]);
                                        // Override data.
                                        if (override.cancelled != 0) {
                                            schedule.cancelled = true;
                                        } else {
                                            schedule.cancelled = false;
                                        }
                                        if (override.weekday) {
                                            schedule.weekday = override.weekday;
                                        } else if (schedule.originalWeekday) {
                                            schedule.weekday = schedule.originalWeekday;
                                        }
                                        if (override.lessonperiodid) {
                                            schedule.lessonperiodid = override.lessonperiodid;
                                        } else if (schedule.originalLessonperiodid) {
                                            schedule.lessonperiodid = schedule.originalLessonperiodid;
                                        }
                                        if (override.classroomid) {
                                            schedule.classroomid = override.classroomid;
                                        } else if (schedule.originalClassroomid) {                                            
                                            schedule.classroomid = schedule.originalClassroomid;
                                        }
                                        if (override.teacherid) {
                                            schedule.teacherid = override.teacherid;
                                        } else if (schedule.originalTeacherid) {                                            
                                            schedule.teacherid = schedule.originalTeacherid;
                                        }
                                        schedule.formids = override.formids;
                                        if (override.continuous == 1) {
                                            schedule.overriddenSince = override.startweek;
                                        }
                                    }
                                }
                                if (self.filter.type === "classroom" && schedule.classroomid != self.filter.id) {
                                    filterMatch = false;
                                }/* else if (self.filter.type === "person" && schedule.teacherid != self.filter.id) {
                                    filterMatch = false;
                                }*/
                            }
                            schedule.timemodified = schedule.weekday != schedule.originalWeekday || schedule.lessonperiodid != schedule.originalLessonperiodid;
                            schedule.classroommodified = schedule.classroomid != schedule.originalClassroomid;
                            schedule.teachermodified = schedule.teacherid != schedule.originalTeacherid;
                            schedule.formsmodified = !_.isEqual(schedule.formids, schedule.originalFormids);
                            if (filterMatch) {
                                self.weeks[weekstart].filteredSchedules[schedule.weekday][schedule.lessonperiodid].push(schedule);
                            }
                        }
                    }
                }
            } else {                
                // DBUG console.log("Not filtering events for", weekstart.format("YYYY-MM-DD"));
            }
        }

        this.filterEvents = function(weekstart) {
            if (!self.weeks[weekstart].filteredEvents || self.filter != self.weeks[weekstart].eventsFilter) {
                // DBUG console.log("Filtering events for", weekstart.format("YYYY-MM-DD"));
                self.weeks[weekstart].eventsFilter = self.filter;
                self.weeks[weekstart].filteredEvents = [];
                self.weeks[weekstart].weekendEvents = [];

                // Only if filter is a person or a form
                if (self.filter.type === 'person' || self.filter.type === 'form' || self.filter.type === 'classroom') {
                    // Iterate through all days of the week
                    for (var weekday = 0; weekday < 7; weekday++) {
                        // Initialize a new events array
                        self.weeks[weekstart].filteredEvents[weekday] = [];
                        // Iterate through every event for the day of the week
                        for (var eventidx in self.weeks[weekstart].events[weekday]) {
                            var event = self.weeks[weekstart].events[weekday][eventidx];
                            if (event.forall === "1") {
                                // If the event is for everyone, add the event to the array
                                //console.log("Event for everybody", event);
                                self.weeks[weekstart].filteredEvents[weekday].push(event);
                            } else if (self.filter.type === 'person') {
                                if (event.people.indexOf(self.filter.id) != -1) {
                                    // If the person participates in the event, add the event to the array
                                    //console.log("Event for participant", event);
                                    self.weeks[weekstart].filteredEvents[weekday].push(event);
                                }
                            } else if (self.filter.type === 'form') {
                                if (event.forms.indexOf(self.filter.id) != -1) {
                                    // If the form participates in the event, add the event to the array
                                    //console.log("Event for participant", event);
                                    self.weeks[weekstart].filteredEvents[weekday].push(event);
                                }
                            } else if (self.filter.type === 'classroom') {
                                if (event.classroomid == self.filter.id) {
                                    // If the event takes place in the classroom, add the event to the array
                                    //console.log("Event for classroom", event);
                                    self.weeks[weekstart].filteredEvents[weekday].push(event);
                                }
                            }
                        }
                    }
                    self.weeks[weekstart].weekendEvents = _.union(self.weeks[weekstart].filteredEvents[6], self.weeks[weekstart].filteredEvents[0]);
                }
            } else {
                // DBUG console.log("Not filtering events for", weekstart.format("YYYY-MM-DD"));
            }
        };

        this.attachParticipants = function(schedule) {
            if (!schedule.participants) {
                schedule.participants = self.listParticipants(schedule.formids);
            }
        };

        this.listParticipants = function(formids) {
            var arr = [];
            for (var formidx in formids) {
                arr.push(self.forms[formids[formidx]]);
            }
            return arr;
        };

        this.getFormsStr = function(schedule) {
            if (!schedule.formsstr) {
                // Put all participating forms' names to an array
                var formstrs = [];
                for (var formidx in schedule.formids) {
                    var form = self.forms[schedule.formids[formidx]];
                    if (form)
                        formstrs.push(form.name);
                }

                // Sort the array
                // TODO breaks when string sorting isn't identical to form integer sorting (eg. 9th grade and 10th grade)
                formstrs.sort();

                // Write all forms to a string using the format: 10.abcde, 11.abcde, 12.abcde
                var formsstr = '';
                var lastGrade = '';
                for (var formstr in formstrs) {
                    if (!formstrs[formstr].match(/^\d+\.*\s*\D$/)) {
                        /* ^\d+\.*\s*\D$ */
                        if (formsstr !== '') {
                            formsstr += ', ';
                        }
                        formsstr += formstrs[formstr];
                    } else {
                        var grade = formstrs[formstr].match(/\d+\.*/)[0];
                        if (lastGrade !== grade) {
                            if (formsstr !== '') {
                                formsstr += ', ';
                            }
                            formsstr += grade;
                            lastGrade = grade;
                        }
                        var parallel = formstrs[formstr].replace(grade, '');
                        formsstr += parallel;
                    }
                }
                schedule.formsstr = formsstr;
            }
            return schedule.formsstr;
        };

        this.getBlockStyle = function(obj) {
            if (obj.hasOwnProperty('teacherid')) {
                if (!self.people[obj.teacherid]) {
                    return null;
                } else if (!obj.teacherid || obj.cancelled) {
                    return {'background-color': '#FFFFFF', 'color': '#000000'};
                } else if (!self.people[obj.teacherid].blockStyle) {
                    var bgcolorhex = self.people[obj.teacherid].color;

                    // Figure out text color on background based on it's contrast
                    var splitcolor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(bgcolorhex);
                    var bgcolor = splitcolor ? {
                        r: parseInt(splitcolor[1], 16),
                        g: parseInt(splitcolor[2], 16),
                        b: parseInt(splitcolor[3], 16)
                    } : null;
                    // Formula from http://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
                    var textcontrast = (bgcolor.r * 0.299 + bgcolor.g * 0.587 + bgcolor.b * 0.114);
                    if (textcontrast <= 120) {
                        self.people[obj.teacherid].brightBg = true;
                    }

                    self.people[obj.teacherid].blockStyle = {'background-color': bgcolorhex};
                }
                return self.people[obj.teacherid].blockStyle;
            } else {
                if (!obj) {
                    return null;
                } else if (!obj.color) {
                    return {'background-color': '#FFFFFF', 'color': '#000000'};
                } else if (!obj.blockStyle) {
                    var bgcolorhex = obj.color;

                    // Figure out text color on background based on it's contrast
                    var splitcolor = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(bgcolorhex);
                    var bgcolor = splitcolor ? {
                        r: parseInt(splitcolor[1], 16),
                        g: parseInt(splitcolor[2], 16),
                        b: parseInt(splitcolor[3], 16)
                    } : null;
                    // Formula from http://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
                    var textcontrast = (bgcolor.r * 0.299 + bgcolor.g * 0.587 + bgcolor.b * 0.114);
                    if (textcontrast <= 120) {
                        obj.brightBg = true;
                    }

                    obj.blockStyle = {'background-color': bgcolorhex};
                }
                return obj.blockStyle;
            }
        };

        // Set next timer
        this.nextTimer = false;
        this.setNextUpdateTimer = function() {
            if (self.nextTimer) {
                // Cancel previous timer
                $timeout.cancel(self.nextTimer);
            }
            var curr = moment();
            var calDate = self.currWeek.dates[self.currWeek.today];

            if (!curr.isSame(calDate, 'week')) {
                // If not this week, we don't need to update on interval
                if (curr.isBefore(calDate)) {
                    // Wait for the week if current time is before week start
                    console.log("1. Next update in " + curr.clone().add(1, 'week').startOf('week').diff(curr), "ms");
                    self.nextTimer = $timeout(self.setNextUpdateTimer, curr.clone().add(1, 'week').startOf('week').diff(curr), true);
                } else {
                    console.log("Cancelled all timers");
                    self.nextTimer = false;
                }
                return;
            } else if (!curr.isSame(calDate, 'day')) {
                // It's this week, but the date has changed - recalc and try again NOW;
                console.log("Recalculating current day");
                self.recalculateCurrentDay();
                self.setNextUpdateTimer();
                return;
            }
            // It's today.
            var period = self.currWeek.periods[self.currWeek.today];

            // If today doesn't have any period.
            // TODO try again tomorrow
            if (!period) {
                console.log("2. Next update in " + curr.clone().add(1, 'day').startOf('day').diff(curr), "ms");
                self.nextTimer = $timeout(self.setNextUpdateTimer, curr.clone().add(1, 'day').startOf('day').diff(curr), true);
                return;
            }
            var lessonperiods = period.lessonperiods;

            var nextUpdate = false;
            self.highlightLesson = false;
            self.highlightBreakBeforeLesson = false;

            for (var id in lessonperiods) {
                var start = moment(lessonperiods[id].starttime, "HH:mm");
                var end = moment(lessonperiods[id].endtime, "HH:mm");
                if (curr.isBetween(start, end)) {
                    nextUpdate = end;
                    self.highlightLesson = id;
                    break;
                } else if (curr.isSameOrBefore(start)) {
                    nextUpdate = start;
                    self.highlightBreakBeforeLesson = id;
                    break;
                }
            }
            // If lessons have already passed
            if (!nextUpdate) {
                // Wait for the start of next day.
                console.log("3. Next update in " + curr.clone().add(1, 'day').startOf('day').diff(curr), "ms");
                self.nextTimer = $timeout(self.setNextUpdateTimer, curr.clone().add(1, 'day').startOf('day').diff(curr), true);
                return;
            }
            console.log("4. Next update in " + nextUpdate.diff(curr), "ms");
            self.nextTimer = $timeout(self.setNextUpdateTimer, nextUpdate.diff(curr), true);
        };

        // Requesting user data
        this.requestUserData = function() {
            console.log('Requesting user data');
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/requestUserData.php'
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error requesting user data: ', response);
                    return;
                }
                if (data.level == -1) {
                    self.user = null;
                    self.auth_url = data.auth_url;
                } else {
                    self.user = data;
                    self.auth_url = null;
                }
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error requesting user data: ', error);
            });
        };

        // Logging out
        this.logout = function() {
            console.log('Logging out');
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/logout.php'
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (data.substring(0, 6) == 'Error:') {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(data)
                            .position('top right')
                            .hideDelay(10000)
                    );
                    console.error('Error logging out: ', data);
                    self.requestUserData();
                    return;
                } else {
                    self.user = null;
                    self.requestUserData();
                }
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error logging out: ', error);
            });
        };

        this.showScheduleEditDialog = function(ev, schedule, period) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            self.attachParticipants(schedule); // Necessary for generating an array
            self.dialogOpen = true;
            $mdDialog.show({
                locals: {data: {prnt: self, schedule: schedule, period: period}},
                controller: DialogController,
                templateUrl: 'templates/scheduleEditDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen
            })
            .then(function(data) {
                // On success
                self.dialogOpen = false;
                // DBUG console.log("Editing schedule data", data);

                /*
                * Inserts or updates holiday into database.
                * Receives REQUEST request with variables: 
                *   weekday - number from 1 to 5 representing the day from monday to friday respectively
                *   lessonperiodid - number representing the lessonperiod of the schedule
                *   classroomid - number representing the id of the classroom
                *   teacherid - number representing the id of the teacher
                *   startweek - the starting week in YYYY-MM-DD format
                *   countinuous - 1 if the override is continuous, 0 if it is not
                *   cancelled - 1 if the schedule is cancelled, 0 if it is not
                * OUTPUT: "holidayid" (or "Error: errormsg")
                *   holidayid - numerical value (ID of the added holiday)
                */

                var fd = new FormData();
                fd.append('datatype', 'override');
                fd.append('scheduleid', schedule.id);
                if (data.changedate) {
                    fd.append('weekday', data.weekday);
                    fd.append('lessonperiodid', data.lessonperiodid);
                }
                if (data.changeclassroom) {
                    fd.append('classroomid', data.classroomid);
                }
                if (data.changeteacher) {
                    fd.append('teacherid', data.teacherid);
                }
                var partForms = [];
                for (var participantIdx in data.participants) {
                    partForms.push(data.participants[participantIdx].id);
                }
                fd.append("forms", JSON.stringify(partForms));
                fd.append('startweek', self.range.start.format('YYYY-MM-DD'));
                fd.append('continuous', data.continuous ? "1" : "0");
                fd.append('cancelled', data.cancelled ? "1" : "0");

                self.insertData(fd);
            }, function() {
                // On cancel
                self.dialogOpen = false;
            });
        }

        this.showTimetableAddDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            self.dialogOpen = true;
            $mdDialog.show({
                locals: {data: self},
                controller: DialogController,
                templateUrl: 'templates/timetableAddDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen
            })
            .then(function(data) {
                // On success
                self.dialogOpen = false;
                // DBUG console.log("Adding timetable data", data);
                var fd = new FormData();
                fd.append('datatype', 'timetable');
                fd.append('create', '1');
                fd.append('file', data.file);
                fd.append('name', data.name);
                fd.append('start', $filter('date')(data.startDate, "yyyy-MM-dd"));
                fd.append('end', $filter('date')(data.endDate, "yyyy-MM-dd"));

                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Tunniplaani lisamine võib aega võtta. Palun oota kuni laadimine on lõppenud.')
                        .position('top right')
                        .hideDelay(10000)
                );

                self.insertData(fd);
            }, function() {
                // On cancel
                self.dialogOpen = false;
            });
        };

        this.showHolidayAddDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            self.dialogOpen = true;
            $mdDialog.show({
                locals: {data: self},
                controller: DialogController,
                templateUrl: 'templates/holidayAddDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen
            })
            .then(function(data) {
                // On success
                self.dialogOpen = false;
                // DBUG console.log("Adding holiday data", data);
                var fd = new FormData();
                fd.append('datatype', 'holiday');
                fd.append('create', '1');
                fd.append('name', data.name);
                fd.append('start', $filter('date')(data.startDate, "yyyy-MM-dd"));
                fd.append('end', $filter('date')(data.endDate, "yyyy-MM-dd"));
                fd.append('description', data.description);

                self.insertData(fd);
            }, function() {
                // On cancel
                self.dialogOpen = false;
            });
        };

        this.showEventAddDialog = function(ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            self.dialogOpen = true;
            $mdDialog.show({
                locals: {data: self},
                controller: DialogController,
                templateUrl: 'templates/eventAddDialog.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen
            })
            .then(function(data) {
                // On success
                self.dialogOpen = false;
                // DBUG console.log("Adding event data", data);
                var fd = new FormData();
                fd.append('datatype', 'event');
                fd.append('create', '1');
                fd.append('name', data.name);
                if (data.description)
                    fd.append('description', data.description);
                if (data.outer) {
                    if (data.location)
                        fd.append('location', data.location);
                } else
                    fd.append('classroomid', data.classroomid);
                var partForms = [];
                var partPeople = [];
                for (var participantIdx in data.participants) {
                    var participant = data.participants[participantIdx];
                    if (participant.type === "form") {
                        partForms.push(participant.id);
                    } else if (participant.type === "person") {
                        partPeople.push(participant.id);
                    }
                }
                if (!data.multiday)
                    data.endDate = data.startDate;
                if (data.allday) {
                    data.startTime = "00:00";
                    data.endTime = "23:59";
                }
                fd.append("forms", JSON.stringify(partForms));
                fd.append("people", JSON.stringify(partPeople));
                fd.append('start', $filter('date')(data.startDate, "yyyy-MM-dd") + " " + data.startTime + ":00");
                fd.append('end', $filter('date')(data.endDate, "yyyy-MM-dd") + " " + data.endTime + ":00");
                fd.append('allday', data.allday ? "1" : "0");
                fd.append('forall', data.forall ? "1" : "0");
                fd.append('color', data.color);
                self.insertData(fd);
            }, function() {
                // On cancel
                self.dialogOpen = false;
            });
        };

        this.insertData = function(fd) {
            console.log("Inserting data", fd);
            self.requesting++;
            $http.post('backend/insert.php', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function(data) {
                self.requesting--;
                if (!angular.isObject(data)) {
                    if (data != undefined && data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error inserting data:', data);
                    return;
                }
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Lisamine või muutmine õnnestus.')
                        .position('top right')
                        .hideDelay(10000)
                );         
                console.log("Data inserted", data);       
                self.reloadTimetableData();
            }).error(function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error inserting data:', error);
            });
        };

        this.removeData = function(datatype, id, ev) {
            var typeStr = "";
            var okAction = "";
            switch (datatype) {
                case 'period':
                    typeStr = 'tunniplaani eemaldada';
                    okAction = 'Eemalda';
                break;
                case 'holiday':
                    typeStr = 'vaheaja eemaldada';
                    okAction = 'Eemalda';
                break;
                case 'event':
                    typeStr = 'sündmuse kustutada';
                    okAction = 'Kustuta';
                break;
            }
            var confirm = $mdDialog.confirm()
                  .title('Oled kindel, et soovid ' + typeStr + '?')
                  .textContent('Seda käsku ei saa tagasi võtta.')
                  .ariaLabel('Eemaldamiskinnitus')
                  .targetEvent(ev)
                  .ok(okAction)
                  .cancel('Tühista');
            $mdDialog.show(confirm).then(function() {
                console.log("Removing data", datatype, id);
                var fd = new FormData();
                fd.append('datatype', datatype);
                fd.append('id', id);
                self.requesting++;
                $http.post('backend/remove.php', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function(data) {
                    self.requesting--;
                    if (!angular.isObject(data)) {
                        if (data != undefined && data.substring(0, 6) == 'Error:') {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent(data)
                                    .position('top right')
                                    .hideDelay(10000)
                            );
                        } else {
                            $mdToast.show(
                                $mdToast.simple()
                                    .textContent('Error.')
                                    .position('top right')
                                    .hideDelay(10000)
                            );
                        }
                        console.error('Error removing data:', data);
                        return;
                    }
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Eemaldamine õnnestus.')
                            .position('top right')
                            .hideDelay(10000)
                    );
                    console.log("Data removed", data);       
                    self.reloadTimetableData();
                }).error(function(error) {
                    self.requesting--;
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                            .position('top right')
                            .hideDelay(10000)
                    );
                    console.error('Error removing data:', error);
                });
            }, function() { 
                // DBUG console.log("Removal cancelled by user:", datatype, id); 
            });
        }



        // Requesting primary data
        this.requestPrimaryData = function() {
            console.log('Requesting primary data');
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q: 'primary'}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error requesting primary data: ', response);
                    return;
                }
                self.forms = data.forms;
                self.people = data.people;
                self.subjects = data.subjects;
                self.classrooms = data.classrooms;

                self.formsArr = $filter('orderBy')($filter('toArray')(data.forms), 'name');

                self.peopleArr = $filter('toArray')(data.people).sort(function(a, b) {
                    var aLast = a.name.slice(a.name.lastIndexOf(' ') + 1);
                    var bLast = b.name.slice(b.name.lastIndexOf(' ') + 1);
                    return aLast.localeCompare(bLast);
                });
                self.subjectsArr = $filter('orderBy')($filter('toArray')(data.subjects), 'name');
                self.classroomsArr = $filter('orderBy')($filter('toArray')(data.classrooms), 'name');

                self.publicFormsArr = $filter('filter')(self.formsArr, {public: "1"});
                self.publicPeopleArr = $filter('filter')(self.peopleArr, {public: "1"});
                self.publicSubjectsArr = $filter('filter')(self.subjectsArr, {public: "1"});
                self.publicClassroomsArr = $filter('filter')(self.classroomsArr, {public: "1"});

                // Construct the filters array
                self.filtersArray = self.formsArr.concat(self.peopleArr, self.subjectsArr, self.classroomsArr);
                self.publicFiltersArray = self.publicFormsArr.concat(self.publicPeopleArr, self.publicSubjectsArr, self.publicClassroomsArr);
                self.filterNormalLength = Object.keys(self.publicFormsArr).length + Object.keys(self.publicPeopleArr).length;

                // Initialize filter
                if ($routeParams.formId) {
                    self.setFilter('form', $routeParams.formId, data.forms[$routeParams.formId].name);
                } else if ($routeParams.personId) {
                    self.setFilter('person', $routeParams.personId, data.people[$routeParams.personId].name);
                } else if ($routeParams.classroomId) {
                    self.setFilter('classroom', $routeParams.classroomId, data.classrooms[$routeParams.classroomId].name);
                } else if ($routeParams.subjectId) {
                    self.setFilter('subject', $routeParams.subjectId, data.subjects[$routeParams.subjectId].name);
                } else {
                    var id = Object.keys(data.forms)[0];
                    self.setFilter('form', id, data.forms[id].name);
                }
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error requesting primary data: ', error);
            });
        };

        // Requesting data in specified time range
        this.requestRangeData = function(start, end) {
            var startdate = start.format('YYYY-MM-DD');
            var enddate = end.format('YYYY-MM-DD');
            console.log('Requesting data for range', startdate, 'to', enddate);
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q:'range', start: startdate, end: enddate}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error requesting range data: ', response);
                    self.weeks[start] = false;
                    return;
                }

                // Add all periods and set holidays and events.
                self.weeks[start].allperiods = {};
                self.weeks[start].periodcount = 0;
                for (var periodid in data.periods) {
                    var period = self.periods[periodid];

                    if (!period) {
                        period = data.periods[periodid];
                        self.periods[periodid] = period;

                        // Parse start and end dates
                        period.start = moment(period.start, 'YYYY-MM-DD');
                        period.end = moment(period.end, 'YYYY-MM-DD');

                        // Initialize period schedules object
                        period.schedules = {form: [], person: [], classroom: [], subject: []};
                    }

                    self.weeks[start].allperiods[periodid] = period;
                    self.weeks[start].periodcount++;

                    var weekPeriodStart = moment.max(period.start, start); // Either start of the week or start of the period in the week
                    var weekPeriodEnd = moment.min(period.end, end); // Either end of the week or end of the period in the week
                    // DBUG console.log("Repeating through", weekPeriodStart.format("YYYY-MM-DD"), weekPeriodEnd.format("YYYY-MM-DD"));
                    for (var m = weekPeriodStart.clone(); !m.isAfter(weekPeriodEnd); m.add(1, 'days')) {
                        // DBUG console.log("\tAdding", m.format('d'), "=", period);
                        self.weeks[start].periods[m.format('d')] = period;
                    }

                    // Setting default lessonperiods
                    self.lessonperiods = period.lessonperiods;

                    // Request filtered data if filter available and previously not requested yet
                    if (self.filter.id != 0 && !period.schedules[self.filter.type][self.filter.id]) {
                        // DBUG console.log("Request filtered data ", weekPeriodStart.format("YYYY-MM-DD"), weekPeriodEnd.format("YYYY-MM-DD"));
                        self.requestFilteredData(period.id, self.filter, start.clone());
                    }
                }
                for (var holidayid in data.holidays) {
                    var holiday = data.holidays[holidayid];

                    // Parse start and end dates
                    holiday.start = moment(holiday.start, 'YYYY-MM-DD');
                    holiday.end = moment(holiday.end, 'YYYY-MM-DD');

                    var weekHolidayStart = moment.max(holiday.start, start); // Either start of the week or start of the holiday in the week
                    var weekHolidayEnd = moment.min(holiday.end, end);
                    for (var m = weekHolidayStart.clone(); !m.isAfter(weekHolidayEnd); m.add(1, 'days')) {
                        var weekday = m.format('d');
                        if (!self.weeks[start].holidays[weekday]) {
                            self.weeks[start].holidays[weekday] = [];
                        }
                        self.weeks[start].holidays[weekday].push(holiday);
                    }
                }
                for (var eventid in data.events) {
                    var event = data.events[eventid];

                    // Parse start and end datetimes
                    event.start = moment(event.start, 'YYYY-MM-DD HH:mm:ss');
                    event.end = moment(event.end, 'YYYY-MM-DD HH:mm:ss');
                    if (!event.location) {
                        event.location = "";
                    }
                    event.duration = {};
                    event.durationCode = {}; // 1 - begins, 2 - lasts, 3 - ends, 4 - begins and ends
                    //{{::event.start}} - {{::event.end}}

                    var weekEventStart = moment.max(event.start, start);
                    var weekEventEnd = moment.min(event.end, end);
                    for (var m = weekEventStart.clone(); !m.isAfter(weekEventEnd); m.add(1, 'days')) {
                        var weekday = m.format('d');
                        if (!self.weeks[start].events[weekday]) {
                            self.weeks[start].events[weekday] = [];
                        }
                        var startDaySame = event.start.isSame(m, 'day');
                        var endDaySame = event.end.isSame(m, 'day');
                        if (startDaySame && endDaySame) {
                            event.duration[weekday] = (event.allday == "1" ? 'terve päev' : (event.start.format('HH:mm') + ' - ' + event.end.format('HH:mm')));
                            event.durationCode[weekday] = 4;
                        } else if (startDaySame) {
                            event.duration[weekday] = 'algab' + (event.allday == "1" ? '' : event.start.format(' HH:mm'));
                            event.durationCode[weekday] = 1;
                        } else if (endDaySame) {
                            // Lõpeb vs Lõppeb - http://keeleabi.eki.ee/index.php?leht=8&id=154
                            event.duration[weekday] = 'lõpeb' + (event.allday == "1" ? '' : event.end.format(' HH:mm'));
                            event.durationCode[weekday] = 3;
                        } else {
                            event.duration[weekday] = '';
                            event.durationCode[weekday] = 2;
                        }
                        self.weeks[start].events[weekday].push(event);
                    }
                    if ((event.durationCode[6] == 1 && event.durationCode[0] == 2) || event.durationCode[6] == 3 || event.durationCode[6] == 4) {
                        event.durationWeekend = 'laup. ' + event.duration[6];
                    } else if (event.durationCode[0] == 1 || (event.durationCode[0] == 3 && event.durationCode[6] == 2) || event.durationCode[0] == 4) {
                        event.durationWeekend = 'pühap. ' + event.duration[0];
                    } else if (event.durationCode[6] == 2 && event.durationCode[0] == 2) {
                        event.durationWeekend = event.duration[0];
                    } else {
                        event.durationWeekend = 'laup. ' + event.duration[6] + '; pühap. ' + event.duration[0];
                    }
                }
                self.weeks[start].filteredEvents = false;
                self.filterEvents(start);
                self.filterSchedules(start);
                self.setNextUpdateTimer();
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error requesting range data:', error);
                self.weeks[start] = false;
            });
        };

        // Requesting data for specified filter and specified period
        var requestingFilteredDataFor = {};
        this.requestFilteredData = function(periodid, filter, weekstart) {
            var filteruid = filter.type + '-' + filter.id;
            if (!requestingFilteredDataFor[periodid]) {
                requestingFilteredDataFor[periodid] = {};
                requestingFilteredDataFor[periodid][filteruid] = [];
            } else if (!requestingFilteredDataFor[periodid][filteruid]) {
                requestingFilteredDataFor[periodid][filteruid] = [];
            } else {
                console.log('Cancelled filtered data request, because request has already been sent.');
                requestingFilteredDataFor[periodid][filteruid].push(weekstart);
                return;
            }

            console.log('Requesting filtered data for period', periodid, 'with filter', filter);
            self.requesting++;
            $http({
                method: 'GET', 
                url: 'backend/request.php',
                params: {q: 'filtered', periodid: periodid, filter_type: filter.type, id: filter.id}
            }).then(function(response) {
                self.requesting--;
                var data = response.data;
                if (!angular.isObject(data)) {
                    if (data.substring(0, 6) == 'Error:') {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent(data)
                                .position('top right')
                                .hideDelay(10000)
                        );
                    } else {
                        $mdToast.show(
                            $mdToast.simple()
                                .textContent('Error.')
                                .position('top right')
                                .hideDelay(10000)
                        );
                    }
                    console.error('Error requesting filtered data:', response);
                    requestingFilteredDataFor[periodid][filteruid] = false;
                    return;
                }
                self.periods[periodid].schedules[filter.type][filter.id] = data;
                // DBUG console.log("Adding week", weekstart.format('YYYY-MM-DD'));
                self.weeks[weekstart].filteredSchedules = false;
                self.filterSchedules(weekstart);

                // Filter schedules for skipped requests as well
                for (var thatweekstartidx in requestingFilteredDataFor[periodid][filteruid]) {
                    var thatweekstart = requestingFilteredDataFor[periodid][filteruid][thatweekstartidx];
                    // DBUG console.log("Adding listener week", thatweekstart.format('YYYY-MM-DD'));
                    self.weeks[thatweekstart].filteredSchedules = false;
                    self.filterSchedules(thatweekstart);
                }
            }, function(error) {
                self.requesting--;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent('Error ' + error.status + ': ' + error.statusText + ' - ' + error.data)
                        .position('top right')
                        .hideDelay(10000)
                );
                console.error('Error requesting filtered data:', error);
                requestingFilteredDataFor[periodid][filteruid] = false;
            });
        };

        // Changing page URL without reload
        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };

        $scope.$watch('isFabOpen', function(isFabOpen) {
            if (isFabOpen) {
                $timeout(function() {
                    $scope.tooltipVisible = $scope.isFabOpen;
                }, 600);
            } else {
                $scope.tooltipVisible = $scope.isFabOpen;
            }
        });
        this.init();
    }
})();

function DialogController($scope, $mdDialog, data) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(data) {
        $mdDialog.hide(data);
    };
    $scope.data = data;
}