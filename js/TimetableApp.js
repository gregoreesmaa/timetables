    /**
     * You must include the dependency on 'ngMaterial' 
     */
    (function() {
        angular
            .module('TimetableApp', ['ngMaterial', 'ngRoute', 'ngMessages', 'colorpicker.module'])
            .config(['$mdThemingProvider', '$mdIconProvider', '$routeProvider', '$mdDateLocaleProvider', function($mdThemingProvider, $mdIconProvider, $routeProvider, $mdDateLocaleProvider){
                // Set first day of week to Monday
                $mdDateLocaleProvider.firstDayOfWeek = 1;

                // URL routes
                $routeProvider
                    .when('/', {
                        templateUrl: 'timetable.html',
                        controller: 'TimetableController',
                        controllerAs: 'tc'
                    })
                    .when('/form/:formId', {
                        templateUrl: 'timetable.html',
                        controller: 'TimetableController',
                        controllerAs: 'tc'
                    })
                    .when('/person/:personId', {
                        templateUrl: 'timetable.html',
                        controller: 'TimetableController',
                        controllerAs: 'tc'
                    })
                    .when('/subject/:subjectId', {
                        templateUrl: 'timetable.html',
                        controller: 'TimetableController',
                        controllerAs: 'tc'
                    })
                    .when('/classroom/:classroomId', {
                        templateUrl: 'timetable.html',
                        controller: 'TimetableController',
                        controllerAs: 'tc'
                    })
                    .when('/print/period/:periodId/form/:formId', {
                        templateUrl: 'timetable_print.html',
                        controller: 'PrintSettingsController',
                        controllerAs: 'pc'
                    })
                    .when('/print/period/:periodId/person/:personId', {
                        templateUrl: 'timetable_print.html',
                        controller: 'PrintSettingsController',
                        controllerAs: 'pc'
                    })
                    .when('/print/period/:periodId/subject/:subjectId', {
                        templateUrl: 'timetable_print.html',
                        controller: 'PrintSettingsController',
                        controllerAs: 'pc'
                    })
                    .when('/print/period/:periodId/classroom/:classroomId', {
                        templateUrl: 'timetable_print.html',
                        controller: 'PrintSettingsController',
                        controllerAs: 'pc'
                    });

                // Icons
                $mdIconProvider
                    .icon("agenda", "./img/agenda.svg", 24)
                    .icon("date_range", "./img/date_range.svg", 24)
                    .icon("event", "./img/event.svg", 24)
                    .icon("file_upload", "./img/ic_file_upload_24px.svg", 24)
                    .icon("add", "./img/add.svg", 24)
                    .icon("close", "./img/ic_close_white_24px.svg", 24)
                    .icon("remove", "./img/ic_remove_white_9px.svg", 9)
                    .icon("arrow_left", "./img/arrow_left.svg", 24)
                    .icon("arrow_right", "./img/arrow_right.svg", 24)
                    .icon("arrow_down", "./img/arrow_down.svg", 24)
                    .icon("print", "./img/print.svg", 24)
                    .icon("import_export", "./img/import_export.svg", 24)
                    .icon("more_vert", "./img/more_vert.svg", 24);

                // Material theme
                $mdThemingProvider.theme('default')
                    .primaryPalette('indigo')
                    .accentPalette('red');
            }])
            .filter('toArray', [function () {
                return function (obj, addKey) {
                    if (!angular.isObject(obj)) return obj;
                    if (addKey === false) {
                        return _.map(Object.keys(obj), function (key) {
                            return obj[key];
                        });
                    } else {
                        return _.map(Object.keys(obj), function (key) {
                            var value = obj[key];
                            return angular.isObject(value) ? Object.defineProperty(value, '$key', { enumerable: false, value: key}) : { $key: key, $value: value };
                        });
                    }
                };
            }])
            .directive('focusMe', ['$timeout', '$parse', function($timeout, $parse) {
                return {
                    link: function(scope, element, attrs) {
                        var model = $parse(attrs.focusMe);
                        scope.$watch(model, function(value) {
                            if (value) { 
                                $timeout(function(){ element[0].focus(); }, 10); 
                                if (typeof value === "string" || value instanceof String) {
                                    if(!scope.$$phase) {
                                        scope.$apply(function () {
                                            if (value === "\b") {
                                                element[0].value = element[0].value.substring(0, element[0].value.length - 1);
                                            } else {
                                                element[0].value += value;
                                            }
                                        });
                                    } else {                                        
                                        if (value === "\b") {
                                            element[0].value = element[0].value.substring(0, element[0].value.length - 1);
                                        } else {
                                            element[0].value += value;
                                        }
                                    }
                                }
                            }
                        });
                        element.bind('blur', function() {
                            model.assign(scope, false);
                        });
                    }
                };
            }])
            .directive('chooseFileButton', function() {
                return {
                    restrict: 'E',
                    link: function (scope, elem, attrs) {
                        var button = elem.find('button');
                        var input = elem.find('input');
                        input.css({ display:'none' });
                        button.bind('click', function() {
                            input[0].click();
                        });
                    }
                };
            })
            .directive('timePicker', function() {
                var times = [];
                var timevalues = [];
                var timevalue = 0;
                for (var hour = 0; hour < 24; hour++) {
                    var hourstr = hour + ":";
                    var hourstrpad = hourstr + "0";
                    for (var minute = 0; minute < 10; minute += 5) {
                        times.push(hourstrpad + minute);
                        timevalues.push(timevalue);
                        timevalue += 5;
                    }
                    for (var minute = 10; minute < 60; minute+=5) {
                        times.push(hourstr + minute);
                        timevalues.push(timevalue);
                        timevalue += 5;
                    }
                }
                var timeToTimevalue = function(time) {
                    var selectedTimedata = time.split(":");
                    return selectedTimedata[0] * 60 + parseInt(selectedTimedata[1], 10);
                };
                return {
                    restrict: 'E',
                    transclude: true,
                    scope: {
                        timeModel: '=ngModel',
                        selectedTime: '@selectedTime',
                        startTime: '@startTime',
                        endTime: '@endTime'
                    },
                    controller: ['$scope', '$parse', function($scope, $parse) {
                        var update = function() {
                            var start = !$scope.startTime ? 0 : _.sortedIndexOf(timevalues, timeToTimevalue($scope.startTime));
                            var end = !$scope.endTime ? timevalues.length : (_.sortedIndexOf(timevalues, timeToTimevalue($scope.endTime)) + 1);                        
                            $scope.times = times.slice(start, end);
                            $scope.selectedIndex = !$scope.selectedTime ? -1 : (_.sortedIndexOf(timevalues, timeToTimevalue($scope.selectedTime)) - start);
                            if ($scope.selectedIndex < 0) {
                                $scope.timeModel = false;
                            }
                        };
                        $scope.$watch('startTime', function(){
                            update();
                        });
                        $scope.$watch('endTime', function(){
                            update();
                        });
                        $scope.$watch('timeModel', function(){
                            if (!$scope.timeModel)
                                return;
                            $scope.selectedTime = $scope.timeModel;
                            console.log("Evaluated selected time", $scope.selectedTime);
                            update();
                        });
                        update();
                    }],
                    template: '<md-select ng-model="timeModel">'
                                + '<md-option ng-repeat="time in times" value="{{::time}}" ng-selected="$index===selectedIndex">'
                                    + '{{::time}}'
                                + '</md-option>'
                            + '</md-select>'
                };
            })
            .directive("file", [function () {
                return {
                    scope: {
                        file: "="
                    },
                    link: function (scope, element, attributes) {
                        element.bind("change", function (changeEvent) {
                            scope.$apply(function () {
                                scope.file = changeEvent.target.files[0];
                            });
                        });
                    }
                }
            }])
            .factory('upper', [function() {
                this.keyDown = function($event) {};
                this.keyUp = function($event) {};
                this.keyPress = function($event) {};
                this.backgroundColor = "#f5f5f5";
                return this;
            }])
            .controller("PageController", ['upper', function(upper) {
                this.upper = upper;
            }]);
    })();