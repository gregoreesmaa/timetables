<?php
	function endsWith($haystack, $needle) {
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
	
	$currenttime = round(microtime(true));
	echo $currenttime."\n";

	$files = scandir ("./");
	foreach ($files as $file) {
		if (endsWith($file, ".xml")) { // check whether it's an xml file	
			$mtime = filemtime($file); // modified time
			$ctime = filectime($file); // creation time
			$time = max($mtime, $ctime); // whatever happened last
			echo $time." ".$file."\n";
		}
	}
?>