-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2016 at 01:28 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `timetables`
--

-- --------------------------------------------------------

--
-- Table structure for table `tt_classrooms`
--

CREATE TABLE IF NOT EXISTS `tt_classrooms` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `alias` varchar(48) NOT NULL,
  `name` varchar(48) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_events`
--

CREATE TABLE IF NOT EXISTS `tt_events` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL,
  `description` text NOT NULL,
  `location` varchar(48) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `creatorid` mediumint(9) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifierid` mediumint(9) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `creatorid` (`creatorid`),
  KEY `modifierid` (`modifierid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_events_forms`
--

CREATE TABLE IF NOT EXISTS `tt_events_forms` (
  `formid` mediumint(9) NOT NULL DEFAULT '0',
  `eventid` mediumint(9) NOT NULL,
  PRIMARY KEY (`formid`,`eventid`),
  KEY `eventid` (`eventid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_events_people`
--

CREATE TABLE IF NOT EXISTS `tt_events_people` (
  `personid` mediumint(9) NOT NULL,
  `eventid` mediumint(9) NOT NULL,
  PRIMARY KEY (`personid`,`eventid`),
  KEY `eventid` (`eventid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_forms`
--

CREATE TABLE IF NOT EXISTS `tt_forms` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `alias` varchar(48) NOT NULL,
  `name` varchar(48) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_groups`
--

CREATE TABLE IF NOT EXISTS `tt_groups` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_groups_people`
--

CREATE TABLE IF NOT EXISTS `tt_groups_people` (
  `personid` mediumint(9) NOT NULL,
  `groupid` mediumint(9) NOT NULL,
  PRIMARY KEY (`personid`,`groupid`),
  KEY `groupid` (`groupid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_holidays`
--

CREATE TABLE IF NOT EXISTS `tt_holidays` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_people`
--

CREATE TABLE IF NOT EXISTS `tt_people` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `alias` varchar(48) NOT NULL,
  `name` varchar(48) NOT NULL,
  `level` mediumint(9) NOT NULL,
  `color` int(11) NOT NULL,
  `avatar_url` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_periods`
--

CREATE TABLE IF NOT EXISTS `tt_periods` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(48) NOT NULL,
  `filename` varchar(32) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_periods_lessonperiods`
--

CREATE TABLE IF NOT EXISTS `tt_periods_lessonperiods` (
  `id` mediumint(9) NOT NULL,
  `starttime` varchar(5) NOT NULL,
  `endtime` varchar(5) NOT NULL,
  `periodid` mediumint(9) NOT NULL,
  PRIMARY KEY (`id`,`periodid`),
  KEY `periodid` (`periodid`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_periods_schedules`
--

CREATE TABLE IF NOT EXISTS `tt_periods_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weekday` tinyint(4) NOT NULL,
  `lessonperiodid` mediumint(9) NOT NULL,
  `classroomid` mediumint(9) DEFAULT NULL,
  `teacherid` mediumint(9) DEFAULT NULL,
  `subjectid` mediumint(9) NOT NULL,
  `periodid` mediumint(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `periodid` (`periodid`),
  KEY `teacherid` (`teacherid`),
  KEY `subjectid` (`subjectid`),
  KEY `classroomid` (`classroomid`),
  KEY `lessonperiodid` (`lessonperiodid`,`periodid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_periods_schedules_forms`
--

CREATE TABLE IF NOT EXISTS `tt_periods_schedules_forms` (
  `formid` mediumint(9) NOT NULL,
  `scheduleid` int(11) NOT NULL,
  KEY `scheduleid` (`scheduleid`),
  KEY `formid` (`formid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_subjects`
--

CREATE TABLE IF NOT EXISTS `tt_subjects` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `alias` varchar(48) NOT NULL,
  `name` varchar(48) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tt_users`
--

CREATE TABLE IF NOT EXISTS `tt_users` (
  `userid` varchar(16) NOT NULL,
  `personid` mediumint(9) NOT NULL,
  PRIMARY KEY (`userid`),
  KEY `personid` (`personid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tt_users_sessions`
--

CREATE TABLE IF NOT EXISTS `tt_users_sessions` (
  `token` varchar(64) NOT NULL,
  `userid` varchar(16) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tt_events`
--
ALTER TABLE `tt_events`
  ADD CONSTRAINT `tt_events_ibfk_2` FOREIGN KEY (`modifierid`) REFERENCES `tt_people` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `tt_events_ibfk_3` FOREIGN KEY (`creatorid`) REFERENCES `tt_people` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `tt_events_forms`
--
ALTER TABLE `tt_events_forms`
  ADD CONSTRAINT `tt_events_forms_ibfk_1` FOREIGN KEY (`formid`) REFERENCES `tt_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_events_forms_ibfk_2` FOREIGN KEY (`eventid`) REFERENCES `tt_events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_events_people`
--
ALTER TABLE `tt_events_people`
  ADD CONSTRAINT `tt_events_people_ibfk_1` FOREIGN KEY (`personid`) REFERENCES `tt_people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_events_people_ibfk_2` FOREIGN KEY (`eventid`) REFERENCES `tt_events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_groups_people`
--
ALTER TABLE `tt_groups_people`
  ADD CONSTRAINT `tt_groups_people_ibfk_1` FOREIGN KEY (`personid`) REFERENCES `tt_people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_groups_people_ibfk_2` FOREIGN KEY (`groupid`) REFERENCES `tt_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_periods_lessonperiods`
--
ALTER TABLE `tt_periods_lessonperiods`
  ADD CONSTRAINT `tt_periods_lessonperiods_ibfk_1` FOREIGN KEY (`periodid`) REFERENCES `tt_periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_periods_schedules`
--
ALTER TABLE `tt_periods_schedules`
  ADD CONSTRAINT `tt_periods_schedules_ibfk_1` FOREIGN KEY (`periodid`) REFERENCES `tt_periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_periods_schedules_ibfk_2` FOREIGN KEY (`teacherid`) REFERENCES `tt_people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_periods_schedules_ibfk_3` FOREIGN KEY (`subjectid`) REFERENCES `tt_subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_periods_schedules_ibfk_5` FOREIGN KEY (`classroomid`) REFERENCES `tt_classrooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_periods_schedules_ibfk_4` FOREIGN KEY (`lessonperiodid`, `periodid`) REFERENCES `tt_periods_lessonperiods` (`id`, `periodid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_periods_schedules_forms`
--
ALTER TABLE `tt_periods_schedules_forms`
  ADD CONSTRAINT `tt_periods_schedules_forms_ibfk_3` FOREIGN KEY (`formid`) REFERENCES `tt_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_periods_schedules_forms_ibfk_4` FOREIGN KEY (`scheduleid`) REFERENCES `tt_periods_schedules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_users`
--
ALTER TABLE `tt_users`
  ADD CONSTRAINT `tt_users_ibfk_1` FOREIGN KEY (`personid`) REFERENCES `tt_people` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tt_users_sessions`
--
ALTER TABLE `tt_users_sessions`
  ADD CONSTRAINT `tt_users_sessions_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `tt_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `user_session_delete_event` ON SCHEDULE EVERY 1 DAY STARTS '2015-12-23 04:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Clean up sessions older than 31 days.' DO DELETE FROM `users_sessions` WHERE `created` < DATE_SUB(NOW(), INTERVAL 31 DAY)$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
